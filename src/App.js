import React, { Component } from 'react'
import { connect } from 'react-redux';
import { HashRouter as Router, Route } from 'react-router-dom';
import { IntlProvider } from 'react-intl';


import FrontPage from './components/FrontPage';
import IntroPage from './components/IntroPage';
import IntroPage2 from './components/IntroPage2';
import PreTest1 from './components/PreTest/Page1';
import PreTest2 from './components/PreTest/Page2';
import App2 from './components/App';
import Results from './components/Results/';
import IndividualResults from './components/individualResults';
import Navbar from './containers/Navbar';

import messages from './intl/messages';
import { setLocale } from './actions/locale';


// import intl provider
import { addLocaleData } from 'react-intl';
import { en } from 'react-intl/locale-data/en.js';
import { es } from 'react-intl/locale-data/es.js';

addLocaleData(en);
addLocaleData(es);

class App extends Component {
  render() {
    if (this.props.lang === undefined) {
      this.props.setLocale('en')
    }
    return (
      <IntlProvider locale={'en'} messages={messages[this.props.lang]}>
        <Router>
          <div>
            <Navbar />
            <Route exact path='/' component={FrontPage} />
            <Route path='/pregunta/:questionIndexFromURL' component={App2} />
            <Route path='/resultados' component={Results} />
            <Route path='/intro' component={IntroPage} />
            <Route path='/intro2/:firstname/:lastname/:email/:code' component={IntroPage2} />
            <Route path='/intro2' component={IntroPage2} />
            <Route path='/pre/1' component={PreTest1} />
            <Route path='/pre/2' component={PreTest2} />
            <Route path='/individualResults/:creativo/:sociable/:valioso/:visible/:visionario/:innovador/:astuto/:intuitivo/:solidario/:generoso/:independiente/:reservado/:email/:username' component={IndividualResults} />
          </div>
        </Router>

      </IntlProvider>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    lang: state.locale.lang
  }
}

export default connect(mapStateToProps, { setLocale })(App);
