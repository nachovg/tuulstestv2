import React, { Component } from 'react'
import { connect } from 'react-redux';
import { setLocale } from '../actions/locale'
import { Link } from 'react-router-dom';

class Navbar extends Component {
  render() {

    return (
      <nav id='navbar' >
        <Link to='/'> <h1>Tuuls</h1></Link>
        <span >
          <a role='button' onClick={() => this.props.setLocale('en')}>EN</a> <b className='yellow'>|</b>
          <a role='button' onClick={() => this.props.setLocale('es')}> ES</a>
        </span>
      </nav>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lang: state.locale.lang
  }
}

export default connect(mapStateToProps, { setLocale })(Navbar)

