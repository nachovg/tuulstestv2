import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

//Actions
import { counter } from '../actions/index';

//Components
import SingleInput from '../components/SingleInput'

//Icons
import CalcIcon from 'react-icons/lib/md/iso'


class InputList extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	inputCallback = (name, value) => {
		//define el State local con 'name' y 'value' del hijo input.
		this.setState({
			[name]: value
		});
	}

	componentWillMount() {
		//se dispara antes de hacer render, por eso busca si hay datos en Local Storage.
		const choice = `choices-${this.props.questionIndexFromURL}`;

		//si y solo si hay datos en localStorage los asigna con setState 
		localStorage.getItem([choice]) &&
			this.setState(JSON.parse(localStorage.getItem([choice])))
	}

	componentWillUpdate(nextProps, nextState) {
		//guarda los valores en Local Storage.
		const choice = `choices-${this.props.questionIndexFromURL}`
		localStorage.setItem([choice], JSON.stringify(nextState));
	}

	counterFromStore = () => (
		this.props.counter(
			//retorna la suma de los valores del obj State a 'counter' del Store.
			Object.values(this.state).reduce((a, b) => a + b, 0)
		)
	)

	componentDidMount() {
		//inicializa el counter con sumatoria de valores.
		return this.counterFromStore()
	}

	componentDidUpdate() {
		//actualiza el counter cuando el usuario hace cambios.
		return this.counterFromStore()
	}

	renderList() {
		let options = this.props.options;
		let englishOptions = this.props.englishOptions;
		return (
			options.map((option, i) => {
				return (
					<li key={i}>
						{this.props.lang === 'en' &&
							<label>{englishOptions[i]}</label>
						}
						{
							this.props.lang === 'es' &&
							<label>{option}</label>

						}
						<SingleInput
							name={option}
							parentState={this.state}
							callbackFromParent={this.inputCallback}
						/>
					</li>
				)
			})
		)
	}

	render() {
		return (
			<div>
				<ul className="circle-container">
					{this.renderList()}
					<li><h4 className='conterCircle'><CalcIcon /><FormattedMessage id='preTest1.counter' defaultMessage='Contador: ' /> {this.props.counterTotal}</h4></li>
				</ul>
				<hr />
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		options: state.options,
		counterTotal: state.counter,
		englishOptions: state.englishOptions,
		lang: state.locale.lang
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		counter: counter
	}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(InputList);

