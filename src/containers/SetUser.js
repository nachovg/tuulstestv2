import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom'
import { FormattedMessage } from 'react-intl';
import { injectIntl } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faClock } from '@fortawesome/free-solid-svg-icons'


//Components
import UserCodes from '../components/UserCodes'

//Actions
import { setuser } from '../actions/index';

class User extends Component {
	constructor(props) {
		super(props)

		this.state = {
			username: '',
			lastName: '',
			code: '',
			termsInput: ''
		}
		this.handleOnChange = this.handleOnChange.bind(this)
		this.handleOnChangeEmail = this.handleOnChangeEmail.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
		this.parentStateCallback = this.parentStateCallback.bind(this)
	}


	parentStateCallback(value, codeValue) {
		if (value === true) {
			this.setState({
				code: codeValue
			})
		} else if (value === false) {
			this.setState({
				code: ''
			})
		}

	}

	handleOnChange(event) {
		this.setState({
			[event.target.name]: event.target.value,
		})
		sessionStorage.setItem([event.target.name], event.target.value, ["lastName"], this.state.lastName, ["code"], this.state.code);
	}

	handleOnChangeEmail(event) {
		console.log(this.state.termsInput)
		if (event.target.value !== '') {
			document.getElementById('codeInput').disabled = false;
		} else {
			document.getElementById('codeInput').disabled = true;
		}


		this.setState({
			[event.target.name]: event.target.value,
		})
		sessionStorage.setItem([event.target.name], event.target.value, ["lastName"], this.state.lastName, ["code"], this.state.code);
	}

	handleOnClick() {
		sessionStorage.setItem(["code"], this.state.code);
		this.props.setuser(this.state)
	}

	componentDidUpdate() {
		return Object.values(this.state).every(i => i !== '') ? "enable" : "disabled";
	}

	componentDidMount() {
		localStorage.clear();
	}

	render() {
		const btnMode = this.componentDidUpdate();
		return (
			<div className='formTest'>
				<div className='user-fields' id='userFields'>
					<div className='iconContainer'>
						<div><FontAwesomeIcon icon={faClock} size="4x" color="#41474a" /><FormattedMessage id='frontPage.p15' defaultMessage='Solo 20 minutos' /></div>
						<div><FontAwesomeIcon icon={faCheck} size="4x" color="#41474a" /><FormattedMessage id='frontPage.p14' defaultMessage='No hay respuestas buenas o malas' />
						</div>
					</div>
					<div>
					</div>
					<div>
						<input
							type='text' placeholder={this.props.intl.formatMessage({ id: 'results.name' })} name='username'
							value={this.state.username} onChange={this.handleOnChange} />
					</div>
					<div>
						<input
							type='text' placeholder={this.props.intl.formatMessage({ id: 'frontPage.lastName' })} name='lastName'
							value={this.state.lastName} onChange={this.handleOnChange} />
					</div>
					<div>
						<input
							type='email' placeholder="E-mail" name='email'
							id='emailCode'
							value={this.state.email} onChange={this.handleOnChangeEmail} />
					</div>
					<div>
						<UserCodes parentStateCallback={this.parentStateCallback} />
					</div>
				</div>
				<div className="termsContainerTest">
					<input className="termsInput" type='checkbox' name="termsInput" onChange={this.handleOnChange} /> <p><FormattedMessage id='frontPage.p17' defaultMessage='Acepto' /> <a href="https://drive.google.com/file/d/1TS56FSQOc3b5OFiXjr3df-kSwg5hgCof/view" ><FormattedMessage id='frontPage.p18' defaultMessage=' Términos y condiciones' /></a></p>
				</div>
				<div id='htButton'>
					<Link
						onClick={this.handleOnClick}
						to={'intro2'}
						className={`btn-amarillo ${btnMode}`}><FormattedMessage id='frontPage.testButton' defaultMessage='Hacer prueba' />
					</Link>

				</div>
				<div id='htButton'>
					<a className={`btn-amarillo`} href="https://tuuls.app/cuenta-de-membresia/tipos-de-suscripcion/" target="_blank;"><FormattedMessage id='frontPage.p16' defaultMessage='Comprar' /></a>
				</div>
			</div>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({ setuser: setuser }, dispatch);
}

const mapStateToProps = (state) => {
	return {
		lang: state.locale.lang
	}
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(User));

