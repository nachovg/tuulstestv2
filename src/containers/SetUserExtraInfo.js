import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import moment from 'moment';
import DatePicker from 'react-date-picker';
import { FormattedMessage } from 'react-intl';
import { injectIntl } from 'react-intl';

//Actions
import { setuser } from '../actions/index';

class ExtraUser extends Component {
  constructor(props) {
    super(props)

    this.state = {
      country: '',
      region: '',
      gender: '',
      education: '',
      status: '',
      employment: '',
      checked: false,
      startDate: moment()
    }

    this.handleOnChange = this.handleOnChange.bind(this)
  }

  handleOnChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    })
    sessionStorage.setItem([event.target.name], event.target.value);
  }

  acepted = (event) => {
    if (!this.state.checked) {
      this.setState({ acepted: 'acepted', checked: true })
    }
    else {
      this.setState({ acepted: '', checked: false })
    }
  }

  componentDidUpdate() {
    return Object.values(this.state).every(i => i !== '') ? "enable" : "disabled";
  }

  selectCountry(val) {
    this.setState({ country: val });
    sessionStorage.setItem(['country'], val);
  }

  selectRegion(val) {
    this.setState({ region: val });
    sessionStorage.setItem(['region'], val);
  }

  handleChange = (selectedOption, event) => {
    this.setState({ [selectedOption.value]: selectedOption.label });
    sessionStorage.setItem([selectedOption.value], selectedOption.label);
  }

  handleData = (selectedOption, event) => {
    this.setState({ [selectedOption.value]: selectedOption.label });
    sessionStorage.setItem([selectedOption.value], selectedOption.id);
  }

  onChange = (date) => {
    this.setState({ date });
    sessionStorage.setItem(["birthday"], date);
  }

  buttonDisabled = () => {
    if (this.componentDidUpdate() === 'disabled') {
      alert('Debe completar el formulario y aceptar los `Términos y condiciones` para poder continuar');
    }
  }

  render() {
    const { country, region } = this.state;
    const { selectedOption } = this.state;
    const btnMode = this.componentDidUpdate();
    return (
      <div className='user-extra-fields'>
        <div className='basicInfo'>
          <div>
            <CountryDropdown
              id='country'
              value={country}
              onChange={(val) => this.selectCountry(val)}
              defaultOptionLabel={this.props.intl.formatMessage({ id: 'introPage.select' })} />
          </div>
          <br />
          <RegionDropdown
            id='country'
            country={country}
            value={region}
            onChange={(val) => this.selectRegion(val)}
            defaultOptionLabel={this.props.intl.formatMessage({ id: 'introPage.region' })}
          />
          <br />
          <br />
          <h4 className='userInfoTitle'><FormattedMessage id='introPage.birthday' defaultMessage='Fecha de nacimiento' /></h4>

          <div>
            <DatePicker
              onChange={this.onChange}
              value={this.state.date}
            />
          </div>
          {/* <input
            type='number' placeholder="Edad" name='age'
            value={this.state.age} onChange={this.handleOnChange} /> */}
          <h4 className='userInfoTitle'><FormattedMessage id='introPage.genre' defaultMessage='Género' /></h4>
          <Select
            name="form-field-name"
            value={this.state.gender}
            searchable={false}
            onChange={this.handleData}
            placeholder={this.state.gender}
            options={[
              { value: 'gender', label: this.props.intl.formatMessage({ id: 'introPage.male' }), id: '1' },
              { value: 'gender', label: this.props.intl.formatMessage({ id: 'introPage.female' }), id: '2' },
              { value: 'gender', label: this.props.intl.formatMessage({ id: 'introPage.other' }), id: '3' },
            ]}
          />
          <h4 className='userInfoTitle'><FormattedMessage id='introPage.education' defaultMessage='Educación' /></h4>
          <Select
            name="form-field-name"
            value={selectedOption}
            searchable={false}
            onChange={this.handleData}
            placeholder={this.state.education}
            options={[
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies01' }), id: '1' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies02' }), id: '2' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies03' }), id: '3' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies04' }), id: '4' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies05' }), id: '5' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies06' }), id: '6' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies07' }), id: '7' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies08' }), id: '8' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies09' }), id: '9' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies10' }), id: '10' },
              { value: 'education', label: this.props.intl.formatMessage({ id: 'introPage.studies11' }), id: '11' }
            ]}
          />
          <h4 className='userInfoTitle'><FormattedMessage id='introPage.status' defaultMessage='Estado Civil' /></h4>
          <Select
            name="form-field-name"
            value={selectedOption}
            searchable={false}
            onChange={this.handleData}
            placeholder={this.state.status}
            options={[
              { value: 'status', label: this.props.intl.formatMessage({ id: 'introPage.status01' }), id: '1' },
              { value: 'status', label: this.props.intl.formatMessage({ id: 'introPage.status02' }), id: '2' },
              { value: 'status', label: this.props.intl.formatMessage({ id: 'introPage.status03' }), id: '3' },
              { value: 'status', label: this.props.intl.formatMessage({ id: 'introPage.status04' }), id: '4' },
              { value: 'status', label: this.props.intl.formatMessage({ id: 'introPage.status05' }), id: '5' },
            ]}
          />

          <h4 className='userInfoTitle'><FormattedMessage id='introPage.employment' defaultMessage='Áreas de trabajo' /></h4>
          <Select
            name="form-field-name"
            value={selectedOption}
            searchable={false}
            onChange={this.handleData}
            placeholder={this.state.employment}
            options={[
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work01' }), id: '1' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work02' }), id: '2' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work03' }), id: '3' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work04' }), id: '4' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work05' }), id: '5' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work06' }), id: '6' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work07' }), id: '7' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work08' }), id: '8' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work09' }), id: '9' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work10' }), id: '10' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work11' }), id: '11' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work12' }), id: '12' },
              { value: 'employment', label: this.props.intl.formatMessage({ id: 'introPage.work13' }), id: '13' }
            ]}
          />
          <br />
          <div onClick={this.buttonDisabled}>
            <Link
              to={'pre/1'}
              className={`btn-amarillo ${btnMode}`}><FormattedMessage id='introPage.nextButton' defaultMessage='Siguiente' />
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setuser: setuser }, dispatch);
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang
  };
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(ExtraUser));

