import React, { Component } from 'react'
import { connect } from 'react-redux'

class CurrentQuestion extends Component {
	render() {
		return (
			<div>
				{this.props.lang === 'es' &&
					<h2>{this.props.question[this.props.questionIndexFromURL]}<hr id='borderYellowCenter' /></h2>
				}
				{this.props.lang === 'en' &&
					<h2>{this.props.englishQuestions[this.props.questionIndexFromURL]}<hr id='borderYellowCenter' /></h2>
				}
			</div>

		)
	}
}

const mapStateToProps = (state) => {
	return {
		question: state.questions,
		englishQuestions: state.englishQuestions,
		lang: state.locale.lang
	}
}

export default connect(mapStateToProps)(CurrentQuestion);