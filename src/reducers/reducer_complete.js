export default function (state = {
  complete: false,
  representation: 0,
  recomendation: 0,
  utility: 0
}, action) {
  switch (action.type) {
    case 'COMPLETE':
      return { ...state, complete: action.value, representation: action.representation, recomendation: action.recomendation, utility: action.utility }
    default:
      return state
  }
}