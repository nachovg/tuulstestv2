export default function (state = {
  match: false
}, action) {
  switch (action.type) {
    case 'CHECKCODESUCCESS':
      return { ...state, match: action.match }
    case 'CHECKCODEFAIL':
      return { ...state, match: action.match }
    default:
      return state
  }
}