export default function (state = {}, action) {
  switch (action.type) {
    case 'SETUSER':
      return { ...state, ...action.user }
    default:
      return state
  }
}