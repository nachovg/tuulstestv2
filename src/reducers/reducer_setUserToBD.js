import {
  SUCCESS,
  FAIL
} from '../actions/index';

export default function (state = { successStatus: false }, action) {
  switch (action.type) {
    case SUCCESS:
      return {
        successStatus: action.success
      }

    case FAIL:
      return {
        successStatus: action.success
      }

    default:
      return state
  }
}