import { combineReducers } from 'redux'

// Reducers
import user from './reducer_user'
import counter from './reducer_counter'
import questions from './reducer_questions'
import englishQuestions from './reducer_questionsEnglish'
import englishOptions from './reducer_optionsEnglish'

import options from './reducer_options'
import success from './reducer_setUserToBD'
import locale from './reducer_locale'
import userCode from './reducer_codeStatus'

const rootReducer = combineReducers({
	user,
	counter,
	questions,
	englishQuestions,
	englishOptions,
	options,
	success,
	locale,
	userCode
});

export default rootReducer;
