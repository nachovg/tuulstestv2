import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// import the root reducer
import rootReducer from './reducers/index';

import questions from './data/questions';
import englishQuestions from './data/questionsEnglish';

import options from './data/options';
import englishOptions from './data/optionsEnglish';


//create an object for the default data
const preloadedState = {
  questions,
  options,
  englishQuestions,
  englishOptions
};

// Para Redux Dev Tools
// const enhancers = compose(
// 	window.devToolsExtension ? window.devToolsExtension() : f => f
// );

const store = createStore(
  rootReducer,
  preloadedState,
  compose(applyMiddleware(thunk), window.devToolsExtension ? window.devToolsExtension() : f => f)
);

if (module.hot) {
  module.hot.accept('./reducers/', () => {
    const nextRootReducer = require('./reducers/index').default;
    store.replaceReducer(nextRootReducer);
  });
}

export default store;
