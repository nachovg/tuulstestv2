const options = [
  "CREATIVE",
  "SOCIABLE",
  "VALUABLE",
  "VISIBLE",
  "VISIONARY",
  "INNOVATIVE",
  "ASTUTE",
  "INTUITIVE ",
  "COOPERATIVE",
  "GENEROUS",
  "INDEPENDENT",
  "RESERVED"
]

export default options;