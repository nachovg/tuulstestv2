const preguntas = [
  "With my family I’m",
  "With my space / things or property, I’m",
  "To encourage and support others, I’m",
  "With my goals and projects, I’m",
  "When I communicate, I’m",
  "With the people I love, I’m",
  "When they know me, others think of me as someone who’s",
  "In general, when I analyze things I’m",
  "When I think of my interests, I’m",
  "I think of my body as",
  "When I think of the human race, I’m",
  "In the spiritual aspects of my life, I’m"
]

export default preguntas;


