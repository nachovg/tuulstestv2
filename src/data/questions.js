const preguntas = [
  "Con mi familia soy",
  "Con mi espacio/cosas o propiedades soy",
  "Para estimular y apoyar a los otros soy",
  "Con mis objetivos y proyectos soy",
  "Cuando me comunico soy",
  "Con la gente que amo soy",
  "Cuando saben de mi, los otros me consideran como alguien",
  "En general cuando analizo las cosas soy",
  "Cuando pienso en mis intereses soy",
  "Puedo describir mi cuerpo como",
  "Cuando pienso en la raza humana soy",
  "En los aspectos espirituales de mi vida soy"
]

export default preguntas;