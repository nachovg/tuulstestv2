const options = [
	"Creativo",
	"Sociable",
	"Valioso",
	"Visible",
	"Visionario",
	"Innovador",
	"Astuto",
	"Intuitivo",
	"Solidario",
	"Generoso",
	"Independiente",
	"Reservado"
]

export default options;