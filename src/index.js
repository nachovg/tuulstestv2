import React from 'react';

import { render } from 'react-dom';
import { injectIntl } from 'react-intl';

// Import css
import './styles/styles.css';

// import react router deps
import { Provider } from 'react-redux';

import App from './App';

// Store
import store from './store';
import { localeSet } from './actions/locale';


if (sessionStorage) {
  store.dispatch(localeSet(sessionStorage.lang));
}

const router = (
  <Provider store={store}>
    <App />
  </Provider>
)

injectIntl(render(router, document.getElementById('root')));
