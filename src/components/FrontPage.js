import React, { Component } from 'react'
import SetUser from '../containers/SetUser';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';

// Data
import faqs from '../data/faqs.json'
import faqsEn from '../data/faqsEn.json'

// Images
import img1 from '../images/frontpage/anika-huizinga-477472.jpg'
import img2 from '../images/frontpage/Tuu2.png'



class Frontpage extends Component {
	render() {

		return (
			<div className="Frontpage">

				<header className='showcase'>
					<div className='halfsContainer'>
						<div className="bigHalf">
							<h1><FormattedMessage id='frontPage.p13' defaultMessage='Descubriendo mi propósito con Impronta-Vital' /></h1>
							<p><FormattedMessage id='frontPage.p12' defaultMessage='Test de preferencia cerebral para descubrir el propósito de vida' />
							</p>
							<hr id='borderWhite' />

							<p className='frontText'>
								<FormattedMessage id='frontPage.p11' defaultMessage='Te entregamos los resultados de tu test en un perfil de 12 animales. Estos animales representan programas biológicos que cumplen un propósito en la naturaleza que tú puedes entender fácilmente sin necesidad de ser experto en biología o neurociencia. Es decir, el propósito de un león, es muy diferente a él de una ballena. ¿Estamos de acuerdo?. ¿Té gustaría saber tu cuál es tu propósito? ¡Prueba ya!' />
							</p>
							<div className='animalsImageContainer'></div>
							<div className='textImpronta'><a href="https://impronta.tuuls.com.co/#/individualResults/15/19/15/6.67/5/10/14/3.33/2/3/7/10/correo@gmail.com/Susana" rel="noopener noreferrer" target="_blank"><b>Ver un test de Impronta-Vital</b></a></div>
						</div>
						<div className='smallHalf'>
							<br />
							<div className="excerpt">
								<SetUser />
							</div>
						</div>
					</div>
				</header>

				<div className="content-wrapper-top">
					<h2><FormattedMessage id='frontPage.fifthP' defaultMessage='Test de Preferencia Cerebral' /><hr id='borderYellowCenter' /></h2>

					<p><FormattedMessage id='frontPage.sixP' defaultMessage='Con nuestro modelo, te enseñamos a usar el máximo potencial del cerebro humano para desarrollar con éxito lo que siempre has soñado.' /></p>
				</div>

				<div className="content-wrapper">

					<div className="grid-container">

						<div className="excerpt">
							<h2><FormattedMessage id='frontPage.seventhP' defaultMessage='¿Qué es impronta-vital?' /><hr id='borderYellow' /></h2>
							<p><FormattedMessage id='frontPage.eightP' defaultMessage='El Test Impronta Vital, es un instrumento de medición que te sirve para saber de manera rápida y efectiva cuál es tu manera preferente de ser y de orientarte en el mundo.' /></p>
							<p><FormattedMessage id='frontPage.ninthP' defaultMessage='Esto se refiere a la forma como obtienes información y datos de tu medio ambiente para procesar contenidos en tu mente y consecuentemente comportarte de una u otra manera. Tu preferencia intuitiva y lo que es más fácil de procesar para ti aplicado a un trabajo, una profesión o cualquier actividad en la que te desarrolles, será con altísima probabilidad la mejor manera de cumplir tus sueños, metas y estar mejor adaptado a tu ambiente y a tu mundo laboral, educativo y/o emocional.' /></p>
						</div>

						<div className="pic1"><img src={img1} alt='' /></div>

						<div className="excerpt">
							<h2 id="saber-mas" ><FormattedMessage id='frontPage.tenthP' defaultMessage='¿Te gustaría saber más?' /><hr id='borderYellow' /></h2>
							<div id="accordion">
								{this.props.lang === 'es' &&
									faqs.map((item, i) => (
										<div className="tab" key={i}>
											<input id={i} type="radio" name="tabs" />
											<label htmlFor={i}>{item[0]}</label>
											<div className="tab-content" dangerouslySetInnerHTML={{ __html: item[1] }} />
										</div>
									)
									)
								}
								{this.props.lang === 'en' &&
									faqsEn.map((item, i) => (
										<div className="tab" key={i}>
											<input id={i} type="radio" name="tabs" />
											<label htmlFor={i}>{item[0]}</label>
											<div className="tab-content" dangerouslySetInnerHTML={{ __html: item[1] }} />
										</div>
									)
									)
								}
							</div>
						</div>

						<div className="pic2"><img src={img2} alt='' /></div>

					</div>

				</div>

				<div className='pdf-download'>
					{this.props.lang === 'es' &&
						<a className='btn-amarillo'
							target='_blank'
							href='https://drive.google.com/open?id=1Fyp9kE8ZRSZwecDETMlQ4_ci5Do5hgSL'
							rel='noopener noreferrer'><FormattedMessage id='frontPage.eleventhP' defaultMessage='Descargar PDF sobre los 12 animales de TUULS' /></a>
					}
				</div>

				<div className="content-wrapper-bottom">
					<h2><FormattedMessage id='frontPage.twelveP' defaultMessage='¿Te gustaría una interpretación personalizada?' /></h2>
					<p><FormattedMessage id='frontPage.thirdteenP' defaultMessage='Escríbenos y pide una cita con nuestros coaches especializados en el tema' /></p>
					<div><a className='btn-amarillo' href='mailto:info@tuuls.com.co'><FormattedMessage id='frontPage.fourthteenP' defaultMessage='Pedir cita online' /></a></div>
				</div>

			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		lang: state.locale.lang
	}
}

export default connect(mapStateToProps)(Frontpage);
