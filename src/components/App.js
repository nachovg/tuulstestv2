import React, { Component } from 'react';


// Components
import CurrentQuestion from '../containers/CurrentQuestion'
import InputList from '../containers/InputList'
import NextQuestion from '../components/NextQuestion'

class App extends Component {

  render() {
    const questionIndex = Number(this.props.match.params.questionIndexFromURL) - 1;
    const nextQuestionURL = Number(this.props.match.params.questionIndexFromURL) + 1;
    return (
      <div className="App container">
        {/* {sessionStorage.getItem('session') === "true" && */}
          <div>
            <CurrentQuestion questionIndexFromURL={questionIndex} />
            <form action={`../#/pregunta/${nextQuestionURL}`}>
              <div className="circle-wrapper">
                <InputList questionIndexFromURL={questionIndex} />
              </div>
              <NextQuestion questionIndexFromURL={questionIndex} />
            </form>
          </div>
        {/* // } */}
        {/* {(sessionStorage.getItem('session') === "false" || sessionStorage.getItem('session') === null) &&
          this.props.history.push("/")
        } */}
      </div>
    );
  }
}

export default App;
