import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import { userCodeCheck } from '../actions/index';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";

class UserCodes extends Component {
	constructor(props) {
		super(props)

		this.state = {
			codes: [],
			user_input: '',
			today: new Date(),
			match: false,
			valid_date: false
		}
		this.handleOnChange = this.handleOnChange.bind(this)
		this.checkMatch = this.checkMatch.bind(this)
		this.btnMode = this.btnMode.bind(this)
	}

	handleOnChange(event) {
		this.setState({
			user_input: event.target.value
		})
		this.checkMatch(event.target.value)
	}

	componentWillReceiveProps(newProps) {
		this.setState({
			match: newProps.match,
		});
		this.props.parentStateCallback(newProps.match, this.state.user_input)
	}

	checkMatch = (codeToMatch) => {
		if (codeToMatch.length === 6) {
			this.props.userCodeCheck(codeToMatch);
		}
	}

	btnMode() {
		if (this.state.match) {
			return 'valid'
		} else if (!this.state.match) {
			return 'no-matched'
		}
		else {
			return null
		}
	}

	render() {
		return (
			<div>
				<input
					type='text'
					disabled
					id='codeInput'
					placeholder={this.props.intl.formatMessage({ id: 'frontPage.code' })}
					onChange={this.handleOnChange}
					className={this.btnMode()}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		match: state.userCode.match,
	};
}

const mapDispatchToProps = dispatch => { return bindActionCreators({ userCodeCheck }, dispatch) };


export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(UserCodes));
