import React, { Component } from "react";
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl';

class Next extends Component {

  render() {
    // Abilita el btn cuando la cuenta es igual a 15
    const buttonMode = this.props.counterTotal === 15 ? "enable" : "disabled";
    return (
      // Ternary operator
      <div>
        {11 - (this.props.questionIndexFromURL) !== 0 &&
          <p><b><FormattedMessage id='preTest1.Answered' defaultMessage='Haz contestado' />{this.props.questionIndexFromURL + 1}</b></p>
        }
        {11 - (this.props.questionIndexFromURL) === 0 &&
          <p><b><FormattedMessage id='preTest1.Answered' defaultMessage='Haz contestado' /> {this.props.questionIndexFromURL + 1} <FormattedMessage id='preTest1.AnsweredAndFinish' defaultMessage='y finalizado' /></b></p>
        }
        {11 - (this.props.questionIndexFromURL) > 0 &&
          <p><b><FormattedMessage id='preTest1.missing' defaultMessage='Te faltan' /> {11 - (this.props.questionIndexFromURL)}</b></p>
        }
        {
          (this.props.question.length - 1) !== this.props.questionIndexFromURL ?
            <button type="submit" className={`btn-amarillo ${buttonMode}`} >
              <FormattedMessage id='preTest1.nextButton' defaultMessage='Siguiente Pregunta' />
            </button> :

            <a href='../#/resultados' className={`btn-amarillo ${buttonMode}`}>
              <FormattedMessage id='questions.results' defaultMessage='Siguiente Pregunta' />
            </a>
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    counterTotal: state.counter,
    question: state.questions,
    lang: state.locale.lang
  };
}

export default connect(mapStateToProps)(Next);
