import React from 'react'
//import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

//SVG images
import svgChat from '../../images/resultados/chat.svg'
import svgComputer from '../../images/resultados/computer.svg'
import svgGraph from '../../images/resultados/graph.svg'
import svgPerson from '../../images/resultados/person.svg'
import svgBrain1 from '../../images/resultados/brainCuadrant1.svg'
import svgBrain2 from '../../images/resultados/brainCuadrant2.svg'
import svgBrain3 from '../../images/resultados/brainCuadrant3.svg'
import svgBrain4 from '../../images/resultados/brainCuadrant4.svg'



const ActividadesComerciales = (props) => {
	return (
		<div className="ActividadesComerciales">
			<h1><FormattedMessage id='results.p24' defaultMessage='Actividades comerciales asociadas al cuadrante' /><hr id='borderYellow' /></h1>
			<p><FormattedMessage id='results.p25' defaultMessage='En esta gráfica aparecen los resultados de tu modo preferente de procesar información asociado a actividades comerciales y trabajos. Aquí tendrás panorámica general de cuál es el tipo de actividad productiva con la cual tu cerebro interactúa y se desarrolla con mayor facilidad. ' /></p>

			<div className='grid-wrapper'>

				<div className='item cuadrante1'>
					<img id='brainCuadrant' src={svgBrain1} alt='cerebro cuadrante 3' width='84px' height='84px' />
					<div className="icon"><img src={svgPerson} alt='cuadrante Uno' /></div>
					{localStorage.preferencia === 'true' &&
						<h2 className='number'>{(props.elementos.Tierra).toFixed(2)}%</h2>
					}
					{localStorage.preferencia !== 'true' &&
						<h2 className='number'>{(props.elementos.Tierra * 100 / 180).toFixed(2)}%</h2>
					}
					<p><FormattedMessage id='results.p26' defaultMessage='Tendencia para empresas de carácter social, educación, industria de producción, manufactura, importación, fundaciones, ONGs, transporte, turismo, trabajo con niños y jóvenes, agro y campo, seguridad y lo legal.' /></p>
				</div>

				<div className='item cuadrante3'>
					<img id='brainCuadrant' src={svgBrain2} alt='cerebro cuadrante 3' width='84px' height='84px' />
					<div className='icon'><img src={svgChat} alt='cuadrante tres' /></div>
					{localStorage.preferencia === 'true' &&
						<h2 className='number'>{(props.elementos.Aire).toFixed(2)}%</h2>
					}
					{localStorage.preferencia !== 'true' &&
						<h2 className='number'>{(props.elementos.Aire * 100 / 180).toFixed(2)}%</h2>
					}
					<p><FormattedMessage id='results.p27' defaultMessage='Tendencia para empresas de servicio al cliente, venta de insumos, distribución, construcción, telecomunicaciones, publicidad, mercados, comercialización, artes, ventas.' /></p>
				</div>

				<div className='item cuadrante2'>
					<img id='brainCuadrant' src={svgBrain3} alt='cerebro cuadrante 3' width='84px' height='84px' />
					<div className='icon'><img src={svgComputer} alt='cuandrante dos' /></div>
					{localStorage.preferencia === 'true' &&
						<h2 className='number'>{(props.elementos.Fuego).toFixed(2)}%</h2>
					}
					{localStorage.preferencia !== 'true' &&
						<h2 className='number'>{(props.elementos.Fuego * 100 / 180).toFixed(2)}%</h2>
					}
					<p><FormattedMessage id='results.p28' defaultMessage='Tendencia para empresas de carácter público, servicios del estado, industrias de extracción, materia prima, salud y holismo, bienestar de todos, toda clase de manejo de información y datos, análisis de estos datos.' /></p>
				</div>


				<div className='item cuadrante4'>
					<img id='brainCuadrant' src={svgBrain4} alt='cerebro cuadrante 3' width='84px' height='84px' />
					<div className='icon'><img src={svgGraph} alt='cuadrante cuatro' /></div>
					{localStorage.preferencia === 'true' &&
						<h2 className='number'>{(props.elementos.Agua).toFixed(2)}%</h2>
					}
					{localStorage.preferencia !== 'true' &&
						<h2 className='number'>{(props.elementos.Agua * 100 / 180).toFixed(2)}%</h2>
					}
					<p><FormattedMessage id='results.p29' defaultMessage='Tendencia para empresas de tecnología, investigación, desarrollo, exportación, instituciones educativas de avanzada, desarrollos técnicos y de procedimiento, salud, seguros, entidades financieras.' /></p>
				</div>

			</div>
		</div>
	)
}

export default ActividadesComerciales