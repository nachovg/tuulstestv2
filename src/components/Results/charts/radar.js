import React from 'react';
import { Radar } from 'react-chartjs-2';


let replaceData = {
  "            ": '', "                    ": '', "                  ": '', "              ": '', " ": '', "  ": '', "   ": '', "      ": '', "       ": '', "        ": '', "         ": '', "          ": ''
};

const RadarChart = (props) => {


  const data = {
    //labels: [a,b,c]  --> formato
    labels(payload) {
      return this['labels'] = [...payload];
    },
    datasets: [
      {
        label: 'Valores porcentuales',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        pointBackgroundColor: 'rgba(255,99,132,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(255,99,132,1)',
        //data: [1,2,3] --> formato
        data(payload) {
          return this['data'] = [...payload];
        }
      }
    ]
  };

  // Le envia la info desde props al Obj 'data'
  data.labels(Object.keys(replaceData));
  data.datasets[0].data(Object.values(props.resultados))

  return (
    <Radar data={data} />
  );
}

export default RadarChart