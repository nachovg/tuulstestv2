import React from 'react';
import { Polar } from 'react-chartjs-2';

const data = {
  datasets: [{
    data(payload) {
        return this['data'] = [...payload];
      },
    backgroundColor: [
      '#795548',
      '#80deea',
      '#03a9f4',
      '#fb8c00'
    ],
    label: 'My dataset' // for legend
  }],
  labels(payload) {
    return this['labels'] = [...payload];
  }
};

const PolarChart = (props) => {
  // Le envia la info desde props al Obj 'data'
  data.labels(Object.keys(props.resultados));
  data.datasets[0].data(Object.values(props.resultados))

  return (
      <div>
        <Polar data={data}/>
      </div>
    );
}

export default PolarChart
