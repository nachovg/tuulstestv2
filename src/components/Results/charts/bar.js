import React from 'react';
import {Bar} from 'react-chartjs-2';

const data = {
  //labels: [a,b,c]  --> formato
  labels(payload) {
    return this['labels'] = [...payload];
  },
  datasets: [
    {
      label: 'Animales',
      backgroundColor: 'rgba(255,99,132,0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      //data: [1,2,3] --> formato
      data(payload) {
        return this['data'] = [...payload];
      }
    }
  ]
};


const BarChart = (props) => {

  // Le envia la info desde props al Obj 'data'
  data.labels(Object.keys(props.resultados));
  data.datasets[0].data(Object.values(props.resultados))

  return (
    <div>
      <h3>Arquetipo = Modelo ideal de algo</h3>
      <Bar
        data={data}
        width={100}
        height={50}
        options={{
          maintainAspectRatio: false
        }}
      />
    </div>
  )
}

export default BarChart