import React from 'react'
import { FormattedMessage } from 'react-intl';


const ArquetipoPrincipal = (props) => {
	const animalesPrincipales =
		//dos primeras posciones del array
		props.animalesOrdenados.slice(0, 2)
			// map para quitar acentos, mayusculas
			.map(animal => animal[0].normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase())
	const builder = (animal, props) => {
		let animalImage = props[animal].animal.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase();
		return (
			<div className='item'>
				<h2 className={`title`}><img src={`/images/animalesPreferentes/${animal}.png`} width={250} alt={animalImage} />{props[animal].animal}</h2>
				<p>{props[animal].relacionArquetipo}</p>
				<p><strong><FormattedMessage id='results.p42' defaultMessage='Programa Biológico:' /> </strong> {props[animal].programa}</p>
				<p><strong><FormattedMessage id='results.p43' defaultMessage='Palabras Relacionadas:' /> </strong> {props[animal].palabras}</p>
				<p><strong><FormattedMessage id='results.p44' defaultMessage='Preguntas asociadas a tu mejor desarrollo laboral:' /> </strong></p>
				<ol>{props[animal].preguntas.map((pregunta, i) => <li key={i}>{pregunta}</li>)}</ol>
				<p><i><FormattedMessage id='results.p45' defaultMessage='(Si respondes que si a estas preguntas, este trabajo o actividad seguramente es para ti)' /></i></p>
				<p><strong><FormattedMessage id='results.p46' defaultMessage='Actividades relacionadas con el arquetipo:' /> </strong> {props[animal].actividades}</p>
				<p><strong><FormattedMessage id='results.p47' defaultMessage='Ambientes favorables:' /> </strong>{props[animal].ambientes.favorables}</p>
				<p><strong><FormattedMessage id='results.p48' defaultMessage='Ambientes desfavorables: ' /></strong>{props[animal].ambientes.desfavorables}</p>
				<p><strong><FormattedMessage id='results.p49' defaultMessage='Fragilidad: ' /></strong> {props[animal].fragilidad}</p>
				<p><strong><FormattedMessage id='results.p50' defaultMessage='Aspectos más negativos asociados al arquetipo: ' /></strong> {props[animal].negativos}</p>
				<p><strong><FormattedMessage id='results.p51' defaultMessage='Profesiones: ' /></strong> {props[animal].profesiones}</p>
				<p><strong><FormattedMessage id='results.p52' defaultMessage='Papel en el grupo: ' /></strong>	{props[animal].grupo}</p>
			</div>
		)
	}

	return (
		<div className="ArquetiposPrincipal">
			<h1><FormattedMessage id='results.p53' defaultMessage='Sistema Preferente con el arquetipo principal' /><hr id='borderYellow' /></h1>
			<div>
				{builder(animalesPrincipales[0], props.datosAnimales)}
				{builder(animalesPrincipales[1], props.datosAnimales)}
			</div>
		</div>
	)
}

export default ArquetipoPrincipal