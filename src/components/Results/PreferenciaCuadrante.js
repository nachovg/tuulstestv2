import React from 'react'
import { FormattedMessage } from 'react-intl';

//SVG images
import svgBrain from '../../images/resultados/brain2.svg'

const findIndex = (key, props) => {
  return (props.elementosOrdenados.findIndex((elemento, i) => elemento[0] === key)) + 1
}

const PrefenciaCuadrante = (props) => {
  return (
    <div className='PrefenciaCuadrante'>
      <h1><FormattedMessage id='results.p11' defaultMessage='Modo de preferencia por cuadrante' /><hr id='borderYellow' /></h1>
      <p><FormattedMessage id='results.p12' defaultMessage='Aquí encontrarás los elementos de la naturaleza ordenados de la manera en son más afines contigo, siendo el 1 el que más se te parece y el 4 el que menos se te parece' /></p>

      <div className='grid-wrapper'>

        <div className={`tierra item index-${findIndex('Tierra', props)}`}>
          <h3><FormattedMessage id='results.p16' defaultMessage='Tierra-Sentidos' /></h3>
          <div className='excerpt'>
            <span className='numero'>{findIndex('Tierra', props)}</span>
            <p><FormattedMessage id='results.p17' defaultMessage='Cuadrante del aquí y el ahora: Tener, desarrollo, dominio, límites, abundancia, soporte, poder' />.</p>
          </div>
        </div>

        <div className={`aire item index-${findIndex('Aire', props)}`}>
          <h3><FormattedMessage id='results.p18' defaultMessage='Aire-Pensamiento' /></h3>
          <div className='excerpt'>
            <span className='numero'>{findIndex('Aire', props)}</span>
            <p><FormattedMessage id='results.p19' defaultMessage='Cuadrante de los cambios de orientación: Comunicación, acción, ir, pensar, cielo, grupo, madurar, idea' />.</p>
          </div>
        </div>

        <div className='pic item'><img src={svgBrain} alt='Modo de preferencia por cuadrante' /></div>

        <div className={`fuego item index-${findIndex('Fuego', props)}`}>
          <h3><FormattedMessage id='results.p20' defaultMessage='Fuego-Intuición' /></h3>
          <div className='excerpt'>
            <span className=''>{findIndex('Fuego', props)}</span>
            <p><FormattedMessage id='results.p21' defaultMessage='Cuadrante de lo inexplorado: Percibir, fuego, concebir, conectar, propósito, regeneración, limpiar, regresar' />.</p>
          </div>
        </div>

        <div className={`agua item index-${findIndex('Agua', props)}`}>
          <h3><FormattedMessage id='results.p22' defaultMessage='Agua-Sentimientos' /></h3>
          <div className='excerpt'>
            <span className='numero'>{findIndex('Agua', props)}</span>
            <p><FormattedMessage id='results.p23' defaultMessage='Cuadrante del fluir sin resistencia: Sentir, fluir, ver, exponer, entender, cambiar, agua, convenir' />.</p>
          </div>
        </div>

      </div>

    </div>
  )
}

export default PrefenciaCuadrante