import React from 'react'
import { FormattedMessage } from 'react-intl';

//SVG images
import svgBrain from '../../images/resultados/brain.svg'

const PreferenciaGlobal = (props) => (
  <div className='PreferenciaGlobal'>
    {/* results.p57
results.p58
results.p59
results.p60
results.p61
results.p62
results.p63
results.p64
results.p65 */}

    <h3><FormattedMessage id='results.p57' /></h3>
    <hr id='borderYellow' />
    <p><FormattedMessage id='results.p58' /></p>
    <h3><FormattedMessage id='results.p59' /></h3>
    <hr id='borderYellow' />
    <p><FormattedMessage id='results.p60' /></p>
    <h3><FormattedMessage id='results.p61' /></h3>
    <hr id='borderYellow' />
    <p><FormattedMessage id='results.p62' /></p>
    <h3><FormattedMessage id='results.p63' /></h3>
    <hr id='borderYellow' />
    <p><FormattedMessage id='results.p64' /></p>
    <br />
    <br />
    <h4 className="Pretest"><FormattedMessage id='results.p65' /></h4>
    <br />
    <br />
    <h1><FormattedMessage id='results.p01' defaultMessage='Modo de preferencia global' />
      <hr id='borderYellow' /></h1>
    <p><FormattedMessage id='results.p02' defaultMessage='Tu cerebro tiene dos maneras básicas o principales de procesar información. Esto es a través de los procesos Basales o Frontales. En este resultado encontrarás los porcentajes que equivalen a cada manera y además asociados a palabras que representan intuitivamente la naturaleza diferente de cada parte, dónde:' /></p>
    <p><strong><FormattedMessage id='results.p03' defaultMessage='Basal:' /></strong> <FormattedMessage id='results.p05' defaultMessage='Tendencia a las reacciones emocionales y motivadas.' /><br />
      <strong><FormattedMessage id='results.p04' defaultMessage='Frontal:' /></strong> <FormattedMessage id='results.p06' defaultMessage='Tendencia a la conducta planeada, anticipada y evaluada racionalmente.' /></p>

    <div className='grid-wrapper'>
      <div className='item'>
        {localStorage.preferencia === 'true' &&
          <span id='resultNumberFrontal'>{(props.elementos.Tierra + props.elementos.Aire).toFixed(2)}%</span>
        }
        {localStorage.preferencia !== 'true' &&
          <span id='resultNumberFrontal'>{((props.elementos.Tierra + props.elementos.Aire) * 100 / 180).toFixed(2)}%</span>
        }
        <h2><FormattedMessage id='results.p07' defaultMessage='Frontal o Expresivo' /></h2>
        <p><FormattedMessage id='results.p08' defaultMessage='Día, movimiento, nutrición, acción, desarrollo, evolución, impacto, suministro, territorio, régimen, tendencia, pensamiento, motivación, sol, función, caliente, cielo, rápido, seco, energía, nacimiento, orden.' /></p>
      </div>
      <div>
        <img src={svgBrain} alt='Modo de preferencia global' />
      </div>
      <div className='item'>
        {localStorage.preferencia === 'true' &&
          <span id='resultNumber'>{(props.elementos.Agua + props.elementos.Fuego).toFixed(2)}%</span>
        }
        {localStorage.preferencia !== 'true' &&
          <span id='resultNumber'>{((props.elementos.Agua + props.elementos.Fuego) * 100 / 180).toFixed(2)}%</span>
        }
        <h2><FormattedMessage id='results.p09' defaultMessage='Basal o impresivo' /></h2>
        <p><FormattedMessage id='results.p10' defaultMessage='Noche, fluir, sentimiento, agua, navegación, redes, prestigio, antigüedad, historias, huella, estrellas, profundo, cósmico, noche, frío, tierra, luna, despacio, humedad, fluido, calmado, conexión.' /></p>
      </div>
    </div>
  </div >
)

export default PreferenciaGlobal

