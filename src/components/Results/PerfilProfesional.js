import React from 'react';
import { FormattedMessage } from 'react-intl';

const PerfilProfesional = (props) => {

	let animalesPrincipales =
		//tres primeras posciones del array
		props.animalesOrdenados.slice(0, 3)
			// map para quitar acentos, mayusculas
			.map(animal => animal[0].normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase())
	return (
		<div className='PerfilProfesional'>
			<h1><FormattedMessage id='results.p40' defaultMessage='Perfil y actividades profesionales asociadas al modo preferente' /><hr id='borderYellow' /></h1>
			<p><FormattedMessage id='results.p41' defaultMessage='Para entender cual es tu mejor area de desarrollo, puedes combinar los dos o tres animales principales  y pensar en una actividad o profesión que reuna estas características.' /></p>
			<div className="grid-wrapper">
				<span><img src={`/images/animales/${animalesPrincipales[0]}.png`} width={150} alt='animal' /></span>
				<span><img src={`/images/animales/${animalesPrincipales[1]}.png`} width={150} alt='animal' /></span>
				<span><img src={`/images/animales/${animalesPrincipales[2]}.png`} width={150} alt='animal' /></span>
			</div>
			<ol>
				<li>
					<strong>{props.datosAnimales[animalesPrincipales[0]].animal}: </strong>
					{props.datosAnimales[animalesPrincipales[0]].profesiones}
				</li>
				<li>
					<strong>{props.datosAnimales[animalesPrincipales[1]].animal}: </strong>
					{props.datosAnimales[animalesPrincipales[1]].profesiones}
				</li>
				<li>
					<strong>{props.datosAnimales[animalesPrincipales[2]].animal}: </strong>
					{props.datosAnimales[animalesPrincipales[2]].profesiones}
				</li>
			</ol>
		</div>

	)
}

export default PerfilProfesional