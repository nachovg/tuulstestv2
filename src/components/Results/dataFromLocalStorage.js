import choicesFromFile from '../../data/options.js';

function sumObjectsByKey() {
  return Array.from(arguments).reduce((a, b) => {
    for (let k in b) {
      if (b.hasOwnProperty(k))
        a[k] = (a[k] || 0) + b[k];
    }
    return a;
  }, {});
}

let localStorageIntoArray = Object.values(localStorage).map(item => JSON.parse(item));

function matchProperties(obj) {
  choicesFromFile.map(choice => {
    //si no existe la propiedad, la define con el valor = 0
    if (!obj.hasOwnProperty(choice)) {
      obj[choice] = 0;
    }
    return null;
  });
  return obj;
}

export let resultados = matchProperties(
  sumObjectsByKey(...localStorageIntoArray)
);


let animals = {};

if (resultados.fromIndividual) {
  animals = {
    León: resultados.Creativo,
    Caballo: resultados.Sociable,
    Colibrí: resultados.Valioso,
    Cisne: resultados.Visible,
    Halcón: resultados.Visionario,
    Serpiente: resultados.Innovador,
    Jaguar: resultados.Astuto,
    Dragón: resultados.Intuitivo,
    Cóndor: resultados.Solidario,
    Ballena: resultados.Generoso,
    Oso: resultados.Independiente,
    Búho: resultados.Reservado
  }
} else {
  animals = {
    León: (resultados.Creativo * 100 / 180).toFixed(2),
    Caballo: (resultados.Sociable * 100 / 180).toFixed(2),
    Colibrí: (resultados.Valioso * 100 / 180).toFixed(2),
    Cisne: (resultados.Visible * 100 / 180).toFixed(2),
    Halcón: (resultados.Visionario * 100 / 180).toFixed(2),
    Serpiente: (resultados.Innovador * 100 / 180).toFixed(2),
    Jaguar: (resultados.Astuto * 100 / 180).toFixed(2),
    Dragón: (resultados.Intuitivo * 100 / 180).toFixed(2),
    Cóndor: (resultados.Solidario * 100 / 180).toFixed(2),
    Ballena: (resultados.Generoso * 100 / 180).toFixed(2),
    Oso: (resultados.Independiente * 100 / 180).toFixed(2),
    Búho: (resultados.Reservado * 100 / 180).toFixed(2)
  }
}

export let animales = animals;


//un nuevo array de (animales) con los resultados ordenados por el valor
export let animalesOrdenados = Object.entries(animales).sort((a, b) => b[1] - a[1]);

export let elementos = {
  Tierra: (resultados.Generoso + resultados.Independiente + resultados.Reservado),
  Aire: (resultados.Creativo + resultados.Sociable + resultados.Valioso),
  Agua: (resultados.Visible + resultados.Visionario + resultados.Innovador),
  Fuego: (resultados.Astuto + resultados.Intuitivo + resultados.Solidario)
}

//un nuevo array de (elementos) con los resultados ordenados por el valor
export let elementosOrdenados = Object.entries(elementos).sort((a, b) => b[1] - a[1]);

