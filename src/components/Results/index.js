import 'regenerator-runtime/runtime';
import React, { Component } from 'react';
import { fetchAllData } from '../../actions/index';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl';
import { injectIntl } from 'react-intl';

import ResultadosBajos from './ResultadosBajos';
//Data from Local Storage
import { resultados, animales, animalesOrdenados, elementos, elementosOrdenados } from './dataFromLocalStorage'
import userData from './dataFromSeccionStorage';

//Data from JSON
import datosAnimales from '../../data/animals.json';
import datosAnimalesEn from '../../data/animalsEn.json';

//Components
import PreferenciaGlobal from './PreferenciaGlobal';
import PrefenciaCuadrante from './PreferenciaCuadrante';
import ActividadesComerciales from './ActividadesComerciales';
import SistemaPreferente from './SistemaPreferente';
import ArquetiposAnimales from './ArquetiposAnimales';
import PerfilProfesional from './PerfilProfesional';
import ArquetipoPrincipal from './ArquetipoPrincipal';

class Results extends Component {
	constructor(props) {
		super(props);
		this.state = {}
	}

	static propTypes = {
		fetchAllData: PropTypes.func,
	};

	static defaultProps = {
		fetchAllData: '',
	}

	componentWillMount() {
		this.setState({
			datosAnimales,
			datosAnimalesEn,
			resultados,
			animales,
			animalesOrdenados,
			elementos,
			elementosOrdenados
		})
	}

	componentDidMount() {
		if (localStorage.individual === 'true') {
			localStorage.setItem(`individual`, 'false');
			window.location.reload();
		} else if (localStorage.preferencia !== 'true' && localStorage.notSend === undefined) {
			this.props.fetchAllData();
		}
	}


	handleOnClick = () => {
		return window.print();
	}

	componentWillReceiveProps = (newProps) => {
		if (newProps.userResState) {
			alert(this.props.intl.formatMessage({ id: 'results.successData' }));
		} else {
			alert(this.props.intl.formatMessage({ id: 'results.failData' }));
		}
	}

	render() {
		let buttonMode = '';
		let gender = '';
		let datosAnimales = '';

		if (this.props.lang === 'en') {
			datosAnimales = this.state.datosAnimalesEn;
		} else {
			datosAnimales = this.state.datosAnimales;
		}

		if (localStorage.preferencia === 'true') {
			buttonMode = "enable";
		} else {
			buttonMode = this.props.complete === true ? "enable" : "disabled";
		}

		if (userData.gender === '1' && this.props.lang === 'es') {
			gender = 'masculino';
		} else if (userData.gender === '1' && this.props.lang === 'en') {
			gender = 'male';
		}
		if (userData.gender === '2' && this.props.lang === 'es') {
			gender = 'femenino';
		} else if (userData.gender === '2' && this.props.lang === 'en') {
			gender = 'female';
		} else if (userData.gender === '3' && this.props.lang === 'es') {
			gender = 'otro';
		} else {
			gender = 'other';
		}

		return (
			<div className="Resultados container divToPrint" id="printable">

				<div className="user-details avoid-this">
					{localStorage.preferencia !== 'true' &&
						<div>
							<div><FormattedMessage id='results.name' defaultMessage='Nombre' />: {userData.username + " " + userData.lastName}</div>
							<div><FormattedMessage id='results.gender' defaultMessage='Sexo' />: {gender}</div>
							{userData.birthday !== undefined &&
								<div><FormattedMessage id='introPage.birthday' defaultMessage='Fecha de nacimiento' />: {userData.birthday.substring(3, 16)}</div>
							}
						</div>
					}
					{localStorage.preferencia === 'true' &&
						<div>
							<div><FormattedMessage id='results.name' defaultMessage='Nombre' />: {localStorage.username}</div>
							<div>Email: {localStorage.email}</div>
						</div>
					}
				</div>

				<PreferenciaGlobal elementos={this.state.elementos} />

				<PrefenciaCuadrante
					elementos={this.state.elementos}
					elementosOrdenados={this.state.elementosOrdenados}
				/>

				<ActividadesComerciales elementos={this.state.elementos} />

				<SistemaPreferente animales={animales} />


				<ArquetiposAnimales
					animales={this.state.animales}
					animalesChart={animales}
					animalesOrdenados={this.state.animalesOrdenados}
					datosAnimales={datosAnimales}
					complete={this.props.complete}
					lang={this.props.lang}
				/>

				<PerfilProfesional
					animalesOrdenados={this.state.animalesOrdenados}
					datosAnimales={datosAnimales}
				/>

				<ArquetipoPrincipal
					animalesOrdenados={this.state.animalesOrdenados}
					datosAnimales={datosAnimales}
				/>
				<ResultadosBajos animalMasBajo={animalesOrdenados[11][0]} lang={this.props.lang} />

				<div className='print-wrapper'>
					<button onClick={this.handleOnClick} className={`btn-gris`}><FormattedMessage id='results.p56' defaultMessage='IMPRIMIR O DESCARGAR' /></button>
				</div>
			</div>
		)
	}
}
function mapStateToProps(state) {
	return {
		lang: state.locale.lang,
		userResState: state.success.successStatus
	};
}

const mapDispatchToProps = dispatch => { return bindActionCreators({ fetchAllData }, dispatch) };



export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Results));