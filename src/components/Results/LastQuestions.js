import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';

import { complete } from '../../actions/index';


class LastQuestions extends Component {
  constructor(props) {
    super(props)

    this.state = {
      valueRepresent: '',
      valueUtility: '',
      valueRecomendation: ''
    }
  }

  questions = (e) => {
    if (e.target.name === 'representacion') {
      this.setState({
        valueRepresent: e.target.id
      })
    }
    else if (e.target.name === 'utilidad') {
      this.setState({
        valueUtility: e.target.id
      })
    }
    else {
      this.setState({
        valueRecomendation: e.target.id
      })
    }
  }

  componentDidUpdate() {
    return Object.values(this.state).every(i => i !== '') ? this.props.complete(this.state.valueRecomendation, this.state.valueUtility, this.state.valueRepresent) : '';
  }

  render() {
    return (
      <div>
        <h2>{this.props.intl.formatMessage({ id: 'lastQuestions.p01' })} <b className='yellow'>PDF</b>{this.props.intl.formatMessage({ id: 'lastQuestions.p07' })} <hr id='borderYellow' /></h2>
        <p>{this.props.intl.formatMessage({ id: 'lastQuestions.p02' })}<b>{this.props.intl.formatMessage({ id: 'lastQuestions.p08' })}</b> {this.props.intl.formatMessage({ id: 'lastQuestions.p09' })}</p>
        <p>{this.props.intl.formatMessage({ id: 'lastQuestions.p03' })}</p>

        <div className='grid-wrapper'>
          <br />
          <div className='item'>
            <p id='preguntasFinal'><b><b className='yellow'>1-</b> {this.props.intl.formatMessage({ id: 'lastQuestions.p04' })}</b></p>
            <div id='lastQuestions'>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='1' checked={this.state.valueRepresent === '1' ? true : false} name={'representacion'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p10' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='2' checked={this.state.valueRepresent === '2' ? true : false} name={'representacion'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p11' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='3' checked={this.state.valueRepresent === '3' ? true : false} name={'representacion'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p12' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='4' checked={this.state.valueRepresent === '4' ? true : false} name={'representacion'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p13' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='5' checked={this.state.valueRepresent === '5' ? true : false} name={'representacion'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p14' })}</p></div>
            </div>
          </div>

          <br />

          <div className='item'>
            <p id='preguntasFinal'><b><b className='yellow'>2-</b> {this.props.intl.formatMessage({ id: 'lastQuestions.p05' })}</b></p>
            <div id='lastQuestions'>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='1' checked={this.state.valueUtility === '1' ? true : false} name={'utilidad'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p10' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='2' checked={this.state.valueUtility === '2' ? true : false} name={'utilidad'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p11' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='3' checked={this.state.valueUtility === '3' ? true : false} name={'utilidad'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p12' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='4' checked={this.state.valueUtility === '4' ? true : false} name={'utilidad'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p13' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='5' checked={this.state.valueUtility === '5' ? true : false} name={'utilidad'} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p14' })}</p></div>
            </div>
          </div>
          <br />

          <div className='item'>
            <p id='preguntasFinal'><b><b className='yellow'>3-</b> {this.props.intl.formatMessage({ id: 'lastQuestions.p06' })}</b></p>
            <div id='lastQuestions'>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='1' checked={this.state.valueRecomendation === '1' ? true : false} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p10' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='2' checked={this.state.valueRecomendation === '2' ? true : false} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p11' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='3' checked={this.state.valueRecomendation === '3' ? true : false} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p12' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='4' checked={this.state.valueRecomendation === '4' ? true : false} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p13' })}</p></div>
              <div className='questionItems'><input type='checkbox' onClick={this.questions} id='5' checked={this.state.valueRecomendation === '5' ? true : false} /><p>{this.props.intl.formatMessage({ id: 'lastQuestions.p14' })}</p></div>
            </div>
          </div>

        </div>
      </div >
    )
  }
}

function mapStateToProps(state) {
  return {
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ complete }, dispatch)
};

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(LastQuestions));
