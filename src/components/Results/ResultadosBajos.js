import React, { Component } from "react";
import animalesBajos from '../../data/animalesBajos.json';
import animalesBajosEn from '../../data/animalesBajosEn.json';
import animalsEn from '../../data/animalsEn.json';

import { FormattedMessage } from 'react-intl';

class ResultadosBajos extends Component {
  constructor(props) {
    super(props)

    this.state = {
      valueRepresent: '',
      valueUtility: '',
      valueRecomendation: ''
    }
  }

  render() {
    let animalBajo = this.props.animalMasBajo;
    let animalBajoImg = this.props.animalMasBajo.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase();
    return (
      <div className="ArquetiposPrincipal">
        <div className='item'>
          <h1><FormattedMessage id='results.p54' defaultMessage='Resultados más bajos' /><hr id='borderYellow' /></h1>
          <p><FormattedMessage id='results.p55' defaultMessage='Podrás mejorar capacidades y oportunidades generales trabajando o perfeccionando aspectos
            relacionados con el arquetipo o programa biológico en el que tu puntuación ha sido la menor.
            Comprender esta dimensión de la experiencia de tu vida, te enriquecerá enormemente y te dará
            mayor espacio para que desarrolles tus dones y habilidades naturales. No quiere decir que
            tengas que volverte experto en algo para lo que naturalmente no eres tan bueno, sino más bien,
            que tengas consciencia de cuál es el aspecto que puede ser más difícil para ti al momento de
            entender, procesar, tomar decisiones, sentir y razonar sobre lo que ves e interpretas del mundo.
            Esta información te dice qué tipo de personas o acompañamiento sería crítico para ti, como un aliado
            estratégico o alguien de quien aprender sus dones naturales. También habla de situaciones en las
            que te puedes ver comprometido, porque se te puede hacer difícil procesar o entender cómo funciona
            determinada situación o discernir de qué se trata una información en particular.' />
          </p>
          <div className='title'>
            <span className={animalBajoImg}><img src={`/images/animales/${animalBajoImg}.png`} width={120} alt='animal' /></span>
            
            { this.props.lang === 'es' &&
            <h2 className={`title ${animalBajoImg}`}>{animalBajo}</h2>}
            
            { this.props.lang === 'en' &&
            <h2 className={`title ${animalBajoImg}`}>{animalsEn[animalBajoImg].animal}</h2>}
          </div>
          { this.props.lang === 'es' &&
          <p><b><FormattedMessage id='results.p42' defaultMessage='Programa Biológico: ' /></b>
          {animalesBajos[animalBajo]}
            <br /><br />
            {animalesBajos[animalBajo + 'Texto']}
          </p>
          }  { this.props.lang === 'en' &&
          <p><b><FormattedMessage id='results.p42' defaultMessage='Programa Biológico: ' /></b>
          {animalesBajosEn[animalBajo]}
            <br /><br />
            {animalesBajosEn[animalBajo + 'Texto']}
          </p>
          }
        </div>
      </div>

    )
  }
}



export default ResultadosBajos;
