import React from 'react'
import ChartRadar from './charts/radar'
import { FormattedMessage } from 'react-intl';

const ArquetiposAnimales = (props) => {
	let english = '';
	if (props.lang === 'en') {
		english = 'En';
	}
	let animalesPrincipales =
		//tres primeras posiciones del array
		props.animalesOrdenados.slice(0, 3)
			// map para quitar acentos, mayusculas
			.map(animal => animal[0].normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase())
	let animalesFlat =
		Object.entries(props.animales)
			// map para quitar acentos, mayusculas y devuelve un array
			.map(animal => [animal[0].normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase(), animal[1]])

	let animalsCircle = Object.entries(props.animalesChart)

		// map para quitar acentos, mayusculas y devuelve un array
		.map(animal => [animal[0].normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase(), animal[1]])
	console.log(props)

	return (
		<div className='ArquetiposAnimales'>
			<h1 className='newCircleTitle'>{<FormattedMessage id='newCircleTitle' />}</h1>
			<hr id='borderYellow' />
			<div className='circle-container resultados-animales newCircle'>
				{
					animalsCircle.map(
						animal => {
							return <li key={animal[0]} >
								<label className={animal[0] + `NewCircle`}>{<FormattedMessage id={animal[0]} />}</label>
								<img src={`/images/animales/${animal[0]}.png`} alt='' width={100} />
							</li>
						})
				}
			</div>

			<div className='newTextsContainer'>
				<p><b>{<FormattedMessage id='leonBio' />} : </b>{<FormattedMessage id='leon' />}</p>
				<p><b>{<FormattedMessage id='caballoBio' />} : </b>{<FormattedMessage id='caballo' />}</p>
				<p><b>{<FormattedMessage id='colibriBio' />} : </b>{<FormattedMessage id='colibri' />}</p>
				<p><b>{<FormattedMessage id='cisneBio' />} : </b>{<FormattedMessage id='cisne' />}</p>
				<p><b>{<FormattedMessage id='halconBio' />} : </b>{<FormattedMessage id='halcon' />}</p>
				<p><b>{<FormattedMessage id='serpienteBio' />} : </b>{<FormattedMessage id='serpiente' />}</p>
				<p><b>{<FormattedMessage id='jaguarBio' />} : </b>{<FormattedMessage id='jaguar' />}</p>
				<p><b>{<FormattedMessage id='dragonBio' />} : </b>{<FormattedMessage id='dragon' />}</p>
				<p><b>{<FormattedMessage id='condorBio' />} : </b>{<FormattedMessage id='condor' />}</p>
				<p><b>{<FormattedMessage id='ballenaBio' />} : </b>{<FormattedMessage id='ballena' />}</p>
				<p><b>{<FormattedMessage id='osoBio' />} : </b>{<FormattedMessage id='oso' />}</p>
				<p><b>{<FormattedMessage id='buhoBio' />} : </b>{<FormattedMessage id='buho' />}</p>
			</div>

			<h1><FormattedMessage id='results.p38' defaultMessage='Arquetipos animales asociados a tu modo preferente' /><hr id='borderYellow' /></h1>
			<p><FormattedMessage id='results.p39' defaultMessage='En esta gráfica puedes ver en porcentajes distintas partes de ti que interactúan para formar la estrategia que usa tu cerebro para entender el mundo y procesar información. Los elementos con puntuaciones más altas son los más usados, los elementos con puntuaciones más bajas los menos usados en tu sistema de preferencia cerebral.' /></p>

			<div className='circle-container resultados-animales'>
				{
					animalesFlat.map(
						animal => {
							switch (animal[0]) {
								case animalesPrincipales[0]:
									return <li className={`${animal[0] + english} top first`} key={animal[0]}><img src={`/images/animales/${animal[0]}.png`} width={150} alt='animal' /><span>{animal[1]}%</span></li>;
								case animalesPrincipales[1]:
									return <li className={`${animal[0] + english} top second`} key={animal[0]}><img src={`/images/animales/${animal[0]}.png`} width={150} alt='animal' /><span>{animal[1]}%</span></li>;
								case animalesPrincipales[2]:
									return <li className={`${animal[0] + english} top third`} key={animal[0]}><img src={`/images/animales/${animal[0]}.png`} width={150} alt='animal' /><span>{animal[1]}%</span></li>;
								default:
									return <li className={animal[0] + english} key={animal[0]} ><span>{animal[1]}%</span>
										<img src={`/images/animales/${animal[0]}.png`} alt='' width={100} />
									</li>
							}
						}
					)
				}
				{
					!props.complete &&
					<div id='radar'>
						< ChartRadar resultados={props.animalesChart} />
					</div>
				}
			</div >


			<ol>
				<li>
					<strong>{props.datosAnimales[animalesPrincipales[0]].animal}: </strong>
					{props.datosAnimales[animalesPrincipales[0]].palabras}
				</li>
				<li>
					<strong>{props.datosAnimales[animalesPrincipales[1]].animal}: </strong>
					{props.datosAnimales[animalesPrincipales[1]].palabras}
				</li>
				<li>
					<strong>{props.datosAnimales[animalesPrincipales[2]].animal}: </strong>
					{props.datosAnimales[animalesPrincipales[2]].palabras}
				</li>
			</ol>

		</div >
	)
}

export default ArquetiposAnimales