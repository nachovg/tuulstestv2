import React from 'react'
import { FormattedMessage } from 'react-intl';

const SistemaPreferente = (props) => {

	return (
		<div className='SistemaPreferente'>
			<h1><FormattedMessage id='results.p30' defaultMessage='Sistema Preferente del Yo' /><hr id='borderYellow' /></h1>
			<div className="excerpt">
				<p><FormattedMessage id='results.p31' defaultMessage='Aquí encontrarás los datos que el TEST arrojó sobre lo que es tu manera principal o preferente de responder, orientarte, ser y estar en el mundo. ' /></p>
				<p><FormattedMessage id='results.p32' defaultMessage='Para entender esta parte del TEST, te describimos los resultados en términos de un animal y al lado del animal encontrarás una palabra que es la manera como lo describe mejor' />.</p>
				<p><FormattedMessage id='results.p33' defaultMessage='Usamos animales como arquetipos para que tu entiendas de manera intuitiva y emocional como te pareces a este animal en particular, siempre desde características de la manera biológica en la que ellos han evolucionado. ' /></p>
				<p><FormattedMessage id='results.p34' defaultMessage='Tu puedes entender esto desde tu instinto,  porque  con todos los mamíferos y vertebrados  compartimos ADN, sistemas cerebrales parecidos, motivaciones e impulsos que siempre nos llevan a tratar de sobrevivir y desarrollarnos de la mejor manera posible según determinados ambientes. ' /></p>
				<p><FormattedMessage id='results.p35' defaultMessage='Son 12 los posibles arquetipos  animales, cada uno representando una particular forma de la vida. Tus resultados arrojarán que tienes una combinación particular de todos estos arquetipos, lo que te hace único y diferente' />.</p>
			</div>
			<h4 className='center'><FormattedMessage id='results.p36' defaultMessage='ARQUETIPO' />&nbsp;<b className='yellow'>&nbsp; = &nbsp;&nbsp;</b>  <FormattedMessage id='results.p37' defaultMessage='MODELO IDEAL DE ALGO' /></h4>
			{/* <ChartRadar resultados={props.animales} /> */}
			<br />
		</div>
	)
}

export default SistemaPreferente