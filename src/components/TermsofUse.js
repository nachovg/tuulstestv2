import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl';


class Terms extends Component {
  render() {
    return (
      <div className='terms'>
        <div>
          {this.props.lang === 'es' &&
            <div>
              < h3 > <FormattedMessage id='terms.p01' defaultMessage='Términos y condiciones' /></h3>
              <br />
              <h3> <FormattedMessage id='terms.p02' defaultMessage='TUULS COLOMBIA S.A.S: MANUAL DE BUENAS PRÁCTICAS PARA EL TRATAMIENTO DE DATOS PERSONALES' /></h3>
              <br />
              <div>
                <b><FormattedMessage id='terms.p03' defaultMessage='Bogotá D.C., 23 de abril 2018' /></b><br />
                <b><FormattedMessage id='terms.p04' defaultMessage='TUULS COLOMBIA S.A.S.' /></b> <FormattedMessage id='terms.p01' defaultMessage='(en adelante también...' />(en adelante también <b>“TUULS”</b>), por medio de este documento publica su Manual de Buenas Prácticas para el Tratamiento de Datos Personales (en adelante el “Manual”), el cual consagra la política respectiva para dicho manejo, cumpliendo así con lo establecido en la Ley 1581 de 2012 y en el Decreto 1377 de 2013. De igual manera, el presente Manual se pone a disposición de todos los titulares de datos personales cuyo contenido sea recolectado, almacenado, depurado, usado, analizado, circulado, actualizado o cruzado por TUULS, con el propósito de que conozcan sus derechos, procedimientos, alcance, finalidad y mecanismos contemplados por TUULS, para el manejo de sus datos personales, previa autorización, por parte de ellos.
                Identificación del responsable de los datos: TUULS COLOMBIA S.A.S., con NIT. 901.164.702-4, es una sociedad domiciliada en la ciudad de Bogotá D.C., ubicada en la calle 118 bis N° 11 D – 07 y con el correo electrónico de contacto susana@tuuls.com.co.

                Principios: el presente Manual para el tratamiento de los datos personales, se desarrollará bajo el amparo de los principios generales de TUULS, a más de los siguientes:

                Autorización previa: en virtud del cual se requiere consentimiento previo, informado e inequívoco del Titular para el tratamiento de sus datos personales;

                Finalidad autorizada: se entenderá aquella que, conforme al presente Manual, corresponde con la calidad del Titular en relación con TUULS;

                Confidencialidad: los datos suministrados por su Titular serán manejados de manera confidencial y no se transmitirán a terceros salvo contar con autorización por parte del Titular, o ante requerimiento por parte de las autoridades competentes;

                Seguridad: TUULS realizará los esfuerzos razonables para evitar el acceso no autorizado por parte de terceros a los datos proporcionados, al igual que su adulteración, pérdida, consulta, uso no autorizado o fraudulento;

                Acceso a la información por parte del Titular: el Titular podrá acceder y tener pleno conocimiento de la información que respecto a él conserva TUULS en cualquier momento;

                Temporalidad: TUULS podrá recolectar, compilar, almacenar, usar o circular los datos personales durante el tiempo que sea razonable y necesario, de acuerdo con la finalidad autorizada;

                Individualidad: será individualizable la información que TUULS conserve respecto de cada uno de los Titulares de datos; y

                Calidad del dato: la información sujeta a Tratamiento debe ser veraz, completa, exacta, actualizada, comprobable y comprensible.

                Tratamiento y finalidades de la información: TUULS, al igual que los encargados o terceros que, en virtud de la ley o un contrato, tengan acceso a los datos personales suministrados por el Titular, los utilizarán con base en lo siguiente:

                Datos de niños, niñas y adolescentes (en adelante “NNA”): su utilización estará circunscrita a datos de naturaleza pública y en todo caso deberá contar con la autorización de los padres o representantes de los menores. Lo anterior con miras a garantizar el interés superior de los NNA, sus derechos fundamentales y demás exigencias constitucionales o legales.

                En caso de que TUULS constate que la autorización recibida para el manejo de datos de NNA no satisface las exigencias constitucionales y legales, hará lo posible para suprimir los datos correspondientes en sus bases de datos y, de no lograrlo, TUULS tomará las medidas para evitar hacer uso de dichos datos.

                Datos de clientes o usuarios: su utilización será la necesaria para que TUULS entregue los productos o preste los servicios contratados de manera adecuada, y para informar a los clientes o usuarios de manera detallada acerca de las actuaciones realizadas en atención a la entrega de productos o prestación de servicios contratados. Adicionalmente, TUULS podrá enviar las facturas correspondientes a las direcciones registradas, y realizar las gestiones de conocimiento de clientes o usuarios y de recaudo que estime necesarias.

                De igual manera, TUULS podrá utilizar los datos personales para satisfacer las garantías correspondientes, y para remitir información comercial acerca de sus productos y servicios, ya sea de manera genérica o personalizada, para lo cual podrá emplear mecanismos de fidelización y conocimiento del cliente o usuario, dentro de los parámetros legales.

                Adicionalmente, TUULS podrá trasmitir los datos personales de los clientes o usuarios a los terceros vinculados contractualmente con TUULS y que, entre otras cosas, permitan efectuar las transacciones electrónicas a que haya lugar, al igual que llevar a cabo la entrega de los productos o la prestación de los servicios adquiridos por el cliente o usuario. Para la trasmisión de los datos, TUULS verificará que la entidad correspondiente satisfaga los requisitos de protección exigidos por la normativa de la República de Colombia.

                Datos de empleados: su utilización estará encaminada al desarrollo de la relación laboral, desde la vinculación y hasta diez (10) años siguientes a la terminación del contrato laboral. De igual manera, podrán emplearse por TUULS o un tercero vinculado legal o contractualmente con ella, para garantizar los pagos de nómina y demás prestaciones, también para cualquier proceso de ingreso e identificación del personal.

                TUULS podrá, en cualquier tiempo, verificar la información personal y académica de sus empleados, con el propósito de determinar la veracidad de la misma, igualmente para corroborar el nivel de formación y experiencia.

                Datos de proveedores: su utilización estará encaminada al aprovisionamiento de servicios o productos que TUULS requiera para su normal funcionamiento, por lo cual comprenderán desde el contacto con el proveedor, hasta la culminación de cualquier posible vínculo contractual.

                Datos de consultores externos: su utilización tendrá como finalidad solicitar apoyo profesional o técnico en materias relacionadas con las actividades de TUULS, para TUULS o para sus clientes o usuarios, con previa autorización de sus Titulares.

                Datos de candidatos a trabajo: su utilización se dará en el marco de procesos de selección de personal, adelantados por TUULS. Para garantizar lo anterior, TUULS podrá: i) Almacenar, procesar y categorizar la información, en los formatos de bases de datos que considere más adecuados; ii) Verificar, corroborar, comprobar, validar y comparar la información suministrada por los Titulares, con cualquier medio legal al cual TUULS tenga acceso; iii) Estudiar, analizar y compartir los datos de los Titulares con los aliados de TUULS, quienes garanticen el respeto del presente Manual y aseguren la confidencialidad de la información, previa autorización expresa del Titular; iv) De ser necesario, por falta de capacidad de TUULS, la información podrá ser trasmitida a un tercero, encargado de su manejo, previa notificación a los Titulares de los datos y con garantías suficientes de confidencialidad y buen manejo, por parte del tercero escogido para este propósito; v) Comunicar al candidato el resultado del proceso de selección en el cual participó; y, vi) Contactar al candidato para futuras convocatorias que se ajusten a su perfil.

                Datos sensibles: su recolección, utilización y tratamiento, entre otros, estará limitada a aquellos casos en los cuales la ley lo autorice, por lo cual se garantizará un acceso limitado y controlado a los mismos, en aras de proteger los derechos al honor e intimidad de los Titulares.

                Finalidades generales: los datos suministrados por los Titulares de los mismos, podrán ser utilizados para el control y la prevención del fraude y de lavado de activos, incluyendo, mas no limitándose, a la consulta en listas restrictivas, tales como la lista OFAC y toda la información necesaria requerida para el SARLAFT. De igual manera, los datos podrán emplearse en las labores administrativas y operacionales de TUULS, pudiéndose trasmitir, previa notificación a los Titulares, a terceros vinculados contractual o legalmente, quienes deberán darles un tratamiento acorde con el presente Manual y/o con la ley.

                TUULS podrá hacer uso de la información recaudada en los procesos de interacción personal o virtual, con el propósito de organizar certámenes de capacitación o actualización, realizar investigaciones, análisis de tendencias, comprensión de procesos sociales, estudios de comportamiento humano, estudios de movimientos sociales, investigaciones en psicopatología, con el propósito de entender e implementar toda clase de acciones que mejoren la vida de las personas en grupos y comunidades. Lo anterior, respetando siempre los derechos al honor e intimidad de los Titulares, sin perjuicio de enriquecer el entendimiento acerca del funcionamiento y de la potenciación de la interacción y desarrollo coordinado de los seres humanos, según sus habilidades, capacidades y necesidades.

                En cualquier momento, TULLS podrá confirmar la información personal suministrada acudiendo a entidades públicas, compañías especializadas o centrales de riesgo, a sus contactos, o al empleador o contratante, así como a sus referencias personales, bancarias o laborales, entre otros. Esta información será tratada por el Responsable del Tratamiento en forma confidencial.

                Centrales de Riesgo: el cliente o usuario autoriza libre y expresamente a TUULS para que, por cualquier medio de información y registros sistematizados, consulte, reporte, administre, suministre y mantenga actualizados los datos referentes a su comportamiento crediticio en lo referente a las obligaciones contenidas en el respectivo contrato con TUULS, incluyendo la facultad, en caso de incumplimiento, de reportarlo a cualquier base de datos de deudores morosos o centrales de riesgo para su publicación por el término establecido por la ley.

                Derechos de los Titulares: los Titulares de los datos personales suministrados, en cualquier tiempo, gozarán de los siguientes derechos: i) Conocer, actualizar y rectificar los datos personales suministrados a TUULS, en su calidad de responsable y encargada del tratamiento; de igual manera, podrá hacer uso de las mismas facultades ante los terceros a quienes se les haya trasmitido la información; ii) Solicitar a TUULS prueba de la autorización para el tratamiento de los datos, salvo en los casos expresamente exceptuados; iii) Ser informado, previa solicitud, acerca del tratamiento dado por TUULS  o los terceros encargados, de los datos personales suministrados; iv) Acudir ante la Superintendencia de Industria y Comercio, para presentar quejas por infracciones al Régimen de Protección de Datos Personales; v) Revocar o modificar la autorización; vi) Solicitar la supresión del dato personal cuando en su manejo se infrinjan los principios, derechos y garantías constitucionales o legales; vii) Acceder gratuitamente a los datos personales que hayan sido tratados por TUULS o terceros encargados; viii) Tener fácil acceso al presente Manual; y, ix) Conocer previa y efectivamente cualquier modificación, actualización o sustitución del presente Manual.
                Derechos de los Titulares frente a datos sensibles: los Titulares de los datos personales, podrán abstenerse de suministrar datos sensibles, tales como los relacionados con su origen racial, convicciones religiosas, filiación política, pertenencia a grupos sindicales u organizaciones sociales, preferencias sexuales, estado de salud, entre otros.
                Atención de peticiones, consultas, quejas o reclamos: los Titulares de los datos personales, gozarán de atención para sus peticiones, consultas, quejas o reclamos, en el correo electrónico susana@tuuls.com.co.

                Procedimiento para el ejercicio de los derechos: el ejercicio de los derechos, se ceñirá al siguiente procedimiento:

                Personas facultadas para el ejercicio: los Titulares de los datos personales, sus causahabientes, sus representantes legales o sus apoderados, previamente acreditados, podrán ejercer los derechos enunciados en el presente Manual y aquellos consagrados constitucional y legalmente.

                Tiempo de respuesta ante la solicitud: TUULS, una vez recibida la solicitud correspondiente, dispondrá de diez (10) días hábiles para tramitarla y dar una respuesta efectiva. En caso de resultar insuficiente ese lapso, TUULS comunicará tal situación al solicitante y dispondrá de máximo cinco (5) días hábiles adicionales para resolver efectivamente.

                Solicitudes incompletas o poco claras: aquellas solicitudes incompletas o poco claras, las cuales no permitan determinar al Titular de los datos o el contenido de la solicitud, deberán ser subsanadas. Para ello, TUULS informará de tal situación al solicitante durante los cinco (5) días hábiles siguientes a la recepción de la solicitud. Si trascurridos dos (2) meses luego de la manifestación hecha por TUULS, el solicitante no ha subsanado la solicitud, se entenderá que hubo desistimiento de la misma y, en consecuencia, se procederá a su archivo, sin que haya lugar a pronunciamiento alguno por parte de TUULS.

                Modificaciones, actualizaciones o sustituciones del Manual: TUULS, una vez vea la necesidad de modificar, actualizar o sustituir el presente Manual, comunicará tal decisión a los Titulares de los datos personales. En dicha comunicación, se anexará la modificación, actualización o sustitución respectiva, para que durante un término de quince (15) días hábiles y antes de la publicación o entrada en vigencia del nuevo Manual, el Titular ejerza sus derechos. Una vez vencido este plazo y vigente el nuevo Manual, se entenderá que existe aceptación del mismo, sin perjuicio de que puedan ejercerse los derechos, en cualquier tiempo.

                Seguridad de la información: TUULS ha adoptado y exige a los terceros encargados, la adopción de medidas razonables para proteger los datos personales suministrados y evitar el acceso a los mismos, por parte de personas no autorizadas. Lo anterior, con el propósito de mantener la integridad de la información y evitar su adulteración o cercenamiento. Para ello ha limitado el acceso a las bases de datos, para ciertos funcionarios, contratistas, representantes o agentes de TUULS.

                Sin perjuicio de lo mencionado, TUULS no se hará responsable por ataques informáticos que pueda sufrir, al igual que se mantendrá indemne respecto de cualquier acción cometida por terceros, con el propósito de evitar los controles y superar las medidas de seguridad adoptas por TUULS o por los terceros encargados del tratamiento de los datos personales. Esto, sin detrimento de que TUULS inicie todas las labores que estén a su alcance para recobrar el control e identificar a los responsables, para que las autoridades tomen las decisiones del caso.

                Vigencia del presente Manual: El presente Manual entra en vigencia a partir de su publicación, el 23 de abril de 2018. Los datos personales recolectados, reposarán en las bases de datos hasta tanto sea necesario para cumplir la finalidad de su recolección, sin perjuicio de que en cualquier tiempo puedan ser retirados por petición del Titular, su causahabiente, representante legal o apoderado.
        <br /><br />
                <b><FormattedMessage id='terms.p05' defaultMessage='TÉRMINOS Y CONDICIONES DE USO DE NUESTRO SITIO WEB' /></b>
                <br />
                <br />
                <FormattedMessage id='terms.p06' defaultMessage='El sitio web que está visitando ...' />El sitio web que está visitando es propiedad de TUULS COLOMBIA S.A.S. (en adelante también la “Compañía”), sociedad colombiana, legalmente constituida el 14 de marzo de 2018, con Número de Identificación Tributaria (NIT.) 901.164.702-4 y cuyo domicilio principal es la ciudad de Bogotá D.C., República de Colombia.
                Por lo anterior, al ingresar, revisar, suministrar datos y comprar en este sitio web usted se compromete a leer, conocer y cumplir los términos y condiciones de uso aquí indicados. De igual manera, se obliga a respetar las políticas de privacidad, de manejo de datos personales de la Compañía y las disposiciones en materia de derechos de autor y de propiedad industrial que rigen en la República de Colombia.
                En todo caso, la Compañía podrá modificar, en cualquier momento, los términos y condiciones de uso o las políticas de privacidad aquí descritas, para lo cual informará y dará a conocer los cambios respectivos, con el propósito de que todas las personas los conozcan y acepten nuevamente.
                Derechos del Usuario
                El Usuario del sitio web de la Compañía gozará de la totalidad de los derechos que reconoce la normativa colombiana en materia de protección al consumidor y protección de datos personales.
                En aras de que dichos derechos se puedan ejercer, la Compañía garantiza que el Usuario podrá presentar quejas, reclamos y sugerencias (PQR) a través de nuestros canales de atención y comunicación establecidos en este sitio web.
                Los PQR serán respondidos con plena atención de los derechos del Usuario y de manera oportuna, según lo establecido en las disposiciones normativas de la República de Colombia.
                Compromiso con la autorización para el manejo de datos personales y seguridad de estos últimos
                La Compañía declara que cuenta con mecanismos de control adecuados para que, dentro del desarrollo de sus actividades, los datos personales de los Usuarios del sitio web, cuyo manejo haya sido autorizado a la Compañía, permanezcan protegidos y los mismos sean empleados para las finalidades contenidas en el Manual de Buenas Prácticas, establecido por la Compañía.
                Lo anterior, con la intención de evitar extravíos, filtraciones, malos manejos o modificaciones inapropiadas, al igual que para garantizar los derechos de los Usuarios.
                Información de los Usuarios
                Sin perjuicio de lo establecido en el Manual de Buenas Prácticas, la Compañía pone de presente que para realizar pagos online, una vez se habilite tal mecanismo, es necesario

                 que el Usuario efectúe el registro correspondiente en el sitio web, lo cual exige el suministro autorizado de información personal y confidencial del Usuario. La autorización se entenderá efectuada, una vez el Usuario acepte el manejo de su información, al hacer clic en el elemento respectivo.
                Dicha información será de uso exclusivo de la Compañía y no será revelada a terceros. Sin embargo, además de las excepciones legales, la Compañía revelará y trasmitirá la información requerida por la plataforma que procese las transacciones electrónicas de pago, previa verificación, por parte de la Compañía, de que dicho tercero cuenta con los mecanismos para proteger los datos personales, en los términos de las disposiciones normativas colombianas.
                Para fortalecer la protección de la transacciones, está prohibido que el Usuario comparta con terceros su contraseña o datos de ingreso al sistema de pagos. Esto, por cuanto dicha información, es de uso exclusivo del Usuario y tiene como finalidad validar las órdenes de compra.
                Adicionalmente, para facilitar la entrega del o los productos o del o los servicios adquiridos por el Usuario, la Compañía hará usos de los datos personales del Usuario y podrá revelar o trasmitir dicha información a terceras personas encargadas de efectuar la entrega de aquello contratado por el Usuario. En todo caso, la Compañía verificará que el tercero cuente con los mecanismos para proteger los datos personales, en los términos de las disposiciones normativas colombianas.
                Desde ya, la Compañía pone de presente a los Usuarios que, en cualquier tiempo, podrán revocar la autorización para el manejo de sus datos personales, al igual que actualizar o modificar los mismos.
                Cualquier Usuario, cuando el sitio web de la Compañía tenga habilitado el canal correspondiente, podrá calificar y expresar sus opiniones sobre los productos que comercializa o los servicios que presta la Compañía. Dicha posibilidad, exigirá la identificación del Usuario.
                Respecto de esto último, la Compañía velará por permitir la libre expresión de los Usuarios; sin embargo, aquellas manifestaciones que resulten denigrantes, ofensivas,

                 discriminatorios o contrarias a las disposiciones normativas colombianas, serán retiradas por el administrador del sitio web y, la Compañía pondrá en conocimiento de las autoridades pertinentes dichas manifestaciones, con el propósito de que se tomen las medidas y decisiones a que haya lugar.
                Disposiciones generales y contractuales
                La Compañía procura y realiza el mejor esfuerzo para que el ingreso a este sitio web sea seguro y la información del Usuario se encuentre resguardada. No obstante, la Compañía no se hace responsable por virus, demoras en la operación o trasmisión, errores tecnológicos, manipulación, invasión o intromisión por parte de terceros no autorizados, al igual que ante cualquier circunstancia que afecte el normal funcionamiento del sitio web. En todo caso, la Compañía hará todo lo que esté a su alcance para corregir, en el menor tiempo posible, cualquier anomalía que se pueda presentar.
                Así, con el propósito de minimizar toda posible contingencia, cualquier transacción que se realice por medio de este sitio web, está sujeta a verificación por parte de la Compañía y a confirmación por parte del Usuario. Aquellas transacciones que se efectúen producto de las circunstancias anómalas mencionadas, estarán sujetas a condición resolutoria.
                De igual manera, la Compañía pone de presente que únicamente, aquellas personas que cuenten con plena capacidad legal, podrán contratar a través de este sitio web. En todo caso, será necesario que el Usuario que desee contratar, sea mayor de edad y se registre con el documento de identidad respectivo.
                Por último, la Compañía agradece que ante cualquier error involuntario o anomalía que se pueda presentar, el usuario comunique tal circunstancia escribiendo al siguiente correo electrónico: info@tuuls.com.co
                Características del equipo para la óptima utilización del sitio web
                Para asegurar una correcta visualización y uso del sitio web de la Compañía se recomienda utilizar los navegadores Internet Explorer 10, Chrome, Firefox 3 o superior y manejar una resolución de pantalla de 1024 x 768 o superior.

                 Perfeccionamiento del contrato
                Los productos, servicios y precios incluidos en este sitio web están dirigidos al público en general, razón por la cual será necesaria la verificación y confirmación por parte de la Compañía respecto de la orden de compra, al igual que del lugar o correo electrónico de entrega y del pago efectuado por el Usuario. Luego de lo anterior, la Compañía expedirá la correspondiente factura de venta y se entenderá perfeccionado el contrato de venta entre el Usuario y la Compañía.
                Así, una vez el Usuario reciba un correo electrónico de confirmación de la orden de compra y del pago del o los productos o servicios adquiridos, al igual que de los costos de envío, cuando hubiere lugar a ellos, y de la fecha aproximada de entrega, la Compañía iniciará las gestiones correspondientes para efectuar oportunamente la entrega respectiva al Usuario.
                Sin perjuicio de lo anterior, la Compañía podrá rechazar cualquier orden de compra cuyo lugar físico de entrega no se encuentre dentro del área de cobertura de la Compañía o esté fuera del territorio colombiano.
                Para aquellos productos o servicios que se contraten por una vía distinta a la de este sitio web, el perfeccionamiento del respectivo contrato, tendrán en cuenta lo convenido de manera específica entre la Compañía y el Usuario.
                Cargos por envío e impuestos
                El Usuario asumirá los costos de transporte del o los productos o servicios adquiridos y el pago de los impuestos a que haya lugar en razón de su compra. En todo caso, el valor del transporte, para entregas físicas, y de los impuestos será informado al momento de generar la orden de compra y podrá variar conforme a la ciudad de entrega. El valor

                 correspondiente al transporte e impuestos, estará claramente discriminado en la documentación final que corrobore la compra y notifique la existencia de un pago exitoso.
                La entrega virtual de productos o servicios no generará costos de transporte para el Usuario y se hará al correo electrónico registrado por el Usuario.
                Medios de pago
                El pago de los productos o servicios que se adquieren a través de este sitio web se hará por medio de transferencia o consignación bancaria a la cuenta de la Compañía, la cual será informada al Usuario dentro del proceso de compra.
                Asimismo, una vez la Compañía habilite el canal respectivo, los pagos podrán realizarse de manera virtual, para lo cual el Usuario podrá hacer uso del sistema de pagos electrónicos que ha contratado la Compañía.
                Desde ya, la Compañía se exime de la responsabilidad por cualquier daño que se pueda generar ante alguna falla en las operaciones o comunicaciones de la plataforma virtual de pagos o de las entidades financieras emisoras de las tarjetas válidas para efectuar los pagos, respecto de aquellos pagos que realice el Usuario a través de los mecanismos virtuales.
                De los productos y servicios ofrecidos
                La Compañía, a través de su sitio web, relaciona los distintos productos y servicios que ofrece, los cuales se encaminan al perfeccionamiento del potencial del cerebro humano, haciendo uso de herramientas sistémicas con base en la neurociencia y en la psicología de arquetipos.
                Cada uno de los productos y servicios que ofrece la Compañía gozan de una completa descripción en el sitio web, de manera tal que el Usuario conozca las características particulares de los productos y servicios.

                 Los productos y servicios que ofrece la Compañía se dividen en:
                Evaluaciones y diagnósticos: herramienta virtual a través de la cual el Usuario podrá contar con una evaluación y diagnóstico basado en el pensamiento sistémico, las neuronas espejo y la inteligencia emocional, al hacer uso de la licencia que otorgue la Compañía para acceder al cuestionario respectivo.
                Las características específicas de este producto serán puestas de presente al Usuario antes de su decisión de compra y de la celebración del contrato respectivo.
                Este producto se entrega de manera virtual por la Compañía.
                Coaching sistémico: servicio de branding que ofrece la Compañía con el propósito de potencializar el producto o el servicio del Usuario.
                Las características específicas de este producto serán puestas de presente al Usuario antes de su decisión de compra y de la celebración del contrato respectivo.
                Este producto se entrega de manera virtual o presencial por la Compañía, según las características de lo contratado por el Usuario.
                Talleres y capacitaciones: experiencia que ofrece la Compañía para que el Usuario tenga una vivencia de desarrollo de su creatividad, liderazgo y potencial.
                Las características específicas de este producto serán puestas de presente al Usuario antes de su decisión de compra y de la celebración del contrato respectivo.
                Este producto se entrega de manera virtual o presencial por la Compañía, según las características de lo contratado por el Usuario.
                •
                •
                •

                 Promesa general de entrega
                La Compañía se compromete para con el Usuario a llevar a cabo la entrega de los productos o servicios contratados en el término establecido, según la naturaleza virtual o presencial de la respectiva entrega.
                Para los casos de entregas virtuales, respecto de un producto adquirido a través de este sitio web, el término máximo de entrega será de cinco (5) horas hábiles, que se comenzarán a contar a partir del momento de aceptación de la orden de compra y de la comprobación del pago.
                En todo caso, la Compañía informará al Usuario, a través del correo de confirmación de la orden de compra y de pago exitoso, el tiempo estimado para efectuar la entrega correspondiente, el cual no excederá de veinte (20) horas hábiles.
                En relación con las entregas presenciales, sus condiciones serán acordadas específicamente entre el Usuario y la Compañía.
                No obstante, ante circunstancias imprevistas e irresistibles, ajenas a la voluntad de la Compañía, que retrasen o imposibiliten la entrega, tales circunstancias se informarán telefónicamente o por vía electrónica al Usuario, con el propósito de lograr la mejor solución para el Usuario.
                De igual manera, la Compañía informa que hace sus mayores esfuerzos para precisar con detalle el inventario existente, de manera tal que el Usuario de este sitio web conozca la disponibilidad de los productos. Sin embargo, toda compra estará sujeta a la disponibilidad del inventario, por lo cual, si al realizar el alistamiento del producto para efectuar su entrega no hay disponibilidad del mismo, la Compañía se comunicará con el Usuario para resolver, de la mejor manera, la situación.
                Política de cambios y devoluciones

                 La Compañía dispone de un plazo para realizar devoluciones de diez (10) días hábiles a partir de la fecha de entrega del pedido. Este, deberá notificar acerca de la devolución antes de que se cumpla el término de los diez (10) días hábiles a través de alguno de nuestros canales de atención, los cuales son:
                - Correo electrónico: [info@tuuls.com.co].
                - Teléfono: [=57 3234653188].
                Para que la solicitud del Usuario pueda atenderse, se requiere que el servicio o el producto no se hayan utilizado. Asimismo, el Usuario deberá entregar el original o fotocopia de la factura de venta o, si no cuenta con dicho documento, el Usuario deberá solicitar a la Compañía una constancia o certificado de compra. La verificación de la existencia de una constancia o del certificado de compra, será efectuada por la Compañía.
                Para aquellos productos que se utilicen de manera virtual, la Compañía verificará que el Usuario no haya hecho uso total o parcial de ellos y, si la verificación es satisfactoria, la Compañía procederá a cancelar la autorización de acceso al Usuario, respecto del producto objeto de devolución.
                Una vez la Compañía verifique la procedencia de la solicitud de devolución del o los productos adquiridos por el Usuario, este último podrá optar por las siguientes alternativas:
                1. Cambio del o los productos: esta alternativa estará sujeta a la disponibilidad de inventario al momento de efectuar el cambio. En todo caso, el cambio podrá realizarse por un producto o productos de igual o inferior valor al del o los productos originales.
                Para aquellos Usuarios que opten por esta alternativa y soliciten un producto o productos de inferior valor al del o los productos originales, la Compañía les

                 reconocerá la diferencia en un cupón que el Usuario podrá hacer efectivo durante los seis (6) meses siguientes, para nuevas compras en este sitio web.
                Si por cuestiones de inventario, la Compañía no cuenta con el producto o los productos que el Usuario solicita para el cambio, la Compañía entregará un cupón por el valor total de la compra original y redimible durante los seis (6) meses siguientes, para que el Usuario lo haga efectivo en futuras compras en este sitio web.
                2. Cupón para realizar nueva compra: esta alternativa permitirá que el Usuario, durante los seis (6) meses siguientes, pueda emplear el valor de su compra original en futuros compras que efectúe en este sitio web.
                3. Reembolso del dinero: esta alternativa únicamente aplica para aquellas devoluciones que se originan en fallas de calidad del o los productos.
                El reembolso del dinero lo efectuará la Compañía, previa verificación de la existencia de la falla de calidad del o los productos, y dependerá del medio utilizado para la compra. Así, el reembolso se hará por medio de una transferencia electrónica o a la tarjeta de crédito utilizada para la compra original.
                Con excepción del reembolso del dinero, el cual aplica por fallas de calidad del o los productos, aquellos productos o servicios que hayan sido personalizados para el Usuario, no serán objeto de devolución.
                Cualquiera de las alternativas anteriores, cuando se den los presupuestos para su aceptación, serán ejecutadas por la Compañía en un lapso que no superará diez (10) días hábiles. Esto, sin perjuicio de que, para el reembolso del dinero, las entidades financieras involucradas empleen un término mayor.
                Derecho de retracto

                 En cabal cumplimiento de la normativa colombiana en materia de protección al consumidor, la Compañía reconoce al Usuario (consumidor) la posibilidad de retractarse de la compra efectuada a través de este sitio web.
                En consecuencia, el Usuario tendrá cinco (5) días hábiles, contados a partir de la entrega del o los productos adquiridos, para proceder con la devolución respectiva en ejercicio del derecho de retracto, siempre que se satisfagan las exigencias legales y el producto o el servicio no hayan sido utilizados por el Usuario, toda vez que lo que ofrece la Compañía es una experiencia o una licencia de único uso, así en una misma licencia se otorguen varios accesos.
                Para efectos de esta devolución en ejercicio del derecho de retracto, el Usuario deberá comunicar tal circunstancia a la Compañía a través de los canales indicados en estos términos y condiciones, al igual que deberá devolver el o los productos adquiridos en las mismas condiciones de recepción de los mismos, para lo cual se tendrá presente el contenido del acápite correspondiente en este documento.
                En caso de que se cumplan las condiciones para que el Usuario ejerza el derecho de retracto, la Compañía efectuará el reembolso del dinero en los términos indicados en este documento.
                En ningún caso, el ejercicio oportuno del derecho de retracto acarreará costo alguno para el Usuario.
                Con el propósito de que el Usuario conozca más acerca de este derecho, la Compañía invita a consultar lo pertinente en la página web de la Superintendencia de Industria y Comercio de Colombia, cuyo enlace es: www.sic.gov.co.
                Reversión del pago
                Las compras efectuadas a través de este sitio web y en las cuales el mecanismo de pago no presencial haya sido empleado sin autorización del titular del mismo o en virtud de un fraude, obliga a los participantes del proceso de pago a efectuar la reversión del mismo, cuando así lo solicite el consumidor.

                 Misma circunstancia opera cuando el servicio o el producto entregados no correspondan a lo solicitado o estén defectuosos.
                Si se da cualquiera de los presupuestos anteriores, el Usuario comunicará la solicitud de reversión del pago al emisor del medio de pago empleado para realizar la compra, quien se pondrá en contacto con los demás participantes del proceso de pago y, conjuntamente, efectuarán la reversión a que haya lugar.
                En todo caso, cuando el Usuario haya recibido el servicio o el producto deberá proceder con la devolución correspondiente a la Compañía, para lo cual deberá ponerse en contacto con los canales de atención de la Compañía, indicados en este documento.
                Política de garantía
                Para hacer uso de la garantía sobre el servicio o el producto adquiridos, será necesario que el Usuario presente el original o fotocopia de la factura de venta o, en caso de no contar con ella, puede solicitar la constancia o certificado de compra a la Compañía.
                De igual manera, la Compañía pone de presente que la garantía de cualquier producto estará sujeta a un diagnóstico, con el propósito de determinar la existencia de la anomalía y su posible causa. En todo caso, la respuesta a cualquier solicitud de garantía será atendida en un tiempo máximo de quince (15) días hábiles.
                La garantía no será procedente si el diagnóstico emitido arroja:
                - La existencia de fuerza mayor o caso fortuito.
                - La existencia de un hecho de un tercero.

                 - El uso indebido del bien por parte del Usuario o consumidor.
                - La inadvertencia del Usuario o consumidor acerca de las instrucciones de uso, cuidado y mantenimiento indicados en el manual del producto y en su garantía.
                Si resulta procedente la garantía del o los productos adquiridos, se efectuará la reparación del o los productos de manera totalmente gratuita. En caso de no proceder la reparación, se procederá con el reembolso del precio pagada por el Usuario, en los términos descritos en este documento.
                Si la falla se repitiere y a elección del Usuario, se procederá a una nueva reparación, la devolución del precio pagado o al cambio del o los productos, en los términos indicados en este documento.
                La Compañía pone de presente que, salvo que el o los productos indiquen una garantía mayor, el término de garantía será el que indique la normativa colombiana vigente al momento de efectuar la compra.
                Una vez expire el término de la garantía contractual o legal, según aplique, el Usuario o consumidor deberá asumir el pago de cualquier revisión, diagnóstico y reparación que requiera el o los productos adquiridos.
                Origen de los fondos
                El Usuario solemnemente declara y sostiene lo siguiente:
                - Los recursos empleados para la compra, provienen de una operación, un oficio, una profesión, una actividad o un negocio lícito.

                 - Los dineros empleados para la compra no fueron obtenidos en virtud de cualquier tipo de conducta que esté consagrada en la ley colombiana como falta constitutiva de infracción penal.
                - Nunca he permitido que terceras personas utilicen mis cuentas o mis tarjetas de crédito o débito para consignar o administrar dineros que desconozca su origen o provengan de conductas contrarias a la ley y especialmente a la ley penal.
                - Eximo a la Compañía de toda responsabilidad por la información errónea, falsa o inexacta que se hubiese suministrado en este documento o por la violación del mismo, de tal suerte que seré el único responsable por ello.
      </div>
            </div>
          }
          {this.props.lang === 'en' &&
            <div>
              <h3>TERMS AND CONDITIONS OF USE OF OUR WEBSITE</h3>
              <br />
              The website you are visiting is the property of TUULS COLOMBIA S.A.S. (hereinafter also the "Company"), a Colombian company, legally incorporated on March 14, 2018, with Tax Identification Number (NIT.) 901.164.702-4 and whose principal address is the city of Bogotá D.C., Republic of Colombia.

              Therefore, by entering, reviewing, providing data and purchasing on this website you agree to read, understand and comply with the terms and conditions of use stated herein. Likewise, it is obliged to respect the privacy policies, the handling of personal data of the Company and the provisions on copyright and industrial property that govern the Republic of Colombia.

              In any case, the Company may modify, at any time, the terms and conditions of use or the privacy policies described herein, for which purpose it shall inform and make known the respective changes, so that all persons may know and accept them again.

              User Rights

              The User of the Company's website shall enjoy all the rights recognized by the Colombian regulations on consumer protection and personal data protection.

              In order to exercise these rights, the Company guarantees that the User may submit complaints, claims and suggestions (PQR) through our customer service and communication channels established on this website.

              The PQR will be answered with full attention to the rights of the User and in a timely manner, as established in the regulatory provisions of the Republic of Colombia.

              Commitment to the authorization for the handling of personal data and security of the latter

              The Company declares that it has adequate control mechanisms to ensure that, in the course of its activities, the personal data of the Users of the website, the management of which has been authorised to the Company, remain protected and are used for the purposes contained in the Manual of Good Practices, established by the Company.
              The foregoing, with the intention of avoiding loss, leaks, mishandling or inappropriate modifications, as well as to guarantee the rights of Users.
              User Information

              Without prejudice to the provisions of the Good Practices Manual, the Company states that in order to make online payments, once such a mechanism is enabled, the User must make the corresponding registration on the website, which requires the authorized provision of personal and confidential information of the User. The authorization will be understood to have been effected once the User accepts the handling of his/her information by clicking on the respective element.

              Such information will be for the exclusive use of the Company and will not be disclosed to third parties. However, in addition to the legal exceptions, the Company will disclose and transmit the information required by the platform that processes electronic payment transactions, after verification by the Company that the third party has the mechanisms to protect personal data in accordance with Colombian regulations.

              In order to strengthen the protection of the transactions, the User is prohibited from sharing his/her password or payment system login data with third parties. This, because this information is for the exclusive use of the User and is intended to validate purchase orders.

              In addition, in order to facilitate the delivery of the product(s) or service(s) purchased by the User, the Company will make use of the User's personal data and may disclose or transmit such information to third parties responsible for delivering the information contracted by the User. In any case, the Company will verify that the third party has the mechanisms in place to protect personal data in accordance with Colombian law.

              Of course, the Company would like to inform Users that, at any time, they may revoke their authorization for the handling of their personal data, as well as update or modify them.

              Any User, when the Company's website has the corresponding channel enabled, may qualify and express their opinions on the products marketed or the services provided by the Company. This possibility will require the identification of the User.

              With respect to the latter, the Company shall ensure that Users are free to express themselves freely, but also to make any denigrating or offensive statements,discriminatory or contrary to Colombian regulations, will be withdrawn by the website administrator and the Company will inform the relevant authorities of such statements, with the purpose of taking the necessary measures and decisions.

              General and contractual provisions

              The Company makes every effort to ensure that the access to this website is secure and that the User's information is protected. Nevertheless, the Company is not responsible for viruses, delays in operation or transmission, technological errors, manipulation, invasion or intrusion by unauthorized third parties, or any other circumstance affecting the normal operation of the website. In any case, the Company will do everything in its power to correct any anomaly that may arise in the shortest possible time.

              Thus, in order to minimize any possible contingency, any transaction made through this website is subject to verification by the Company and confirmation by the User. Those transactions that are made as a result of the aforementioned anomalous circumstances shall be subject to a resolutory condition.

              Likewise, the Company states that only those persons with full legal capacity may contract through this website. In any case, it will be necessary for the User who wishes to contract to be of legal age and to register with the respective identity document.

              Finally, the Company appreciates that in the event of any involuntary error or anomaly that may arise, the user may communicate this circumstance by writing to the following e-mail address: info@tuuls.com.co

              Equipment features for optimal use of the website

              To ensure proper viewing and use of the Company's website, we recommend using Internet Explorer 10, Chrome, Firefox 3 or higher browsers and a screen resolution of 1024 x 768 or higher.

              Conclusion of the contract

              The products, services and prices included in this website are aimed at the general public, which is why it will be necessary for the Company to verify and confirm the purchase order, as well as the place or e-mail address of delivery and payment made by the User. After the foregoing, the Company shall issue the corresponding sales invoice and the sales contract between the User and the Company shall be deemed to have been executed.

              Thus, once the User receives an e-mail confirming the purchase order and the payment of the purchased product or services, as well as the shipping costs, if any, and the approximate delivery date, the Company will begin the corresponding steps to make timely delivery to the User.

              Notwithstanding the foregoing, the Company may reject any purchase order whose physical place of delivery is not within the Company's coverage area or is outside Colombian territory.

              For those products or services that are contracted by a means other than that of this website, the perfection of the respective contract shall take into account the specific agreement between the Company and the User.



              Shipping Charges and Taxes

              The User will assume the costs of transporting the product or services purchased and the payment of any taxes incurred as a result of their purchase. In any case, the value of the transport, for physical deliveries, and taxes will be informed at the time of generating the purchase order and may vary according to the city of delivery. The value of the transport and taxes will be clearly discriminated in the final documentation that corroborates the purchase and notifies the existence of a successful payment.

              The virtual delivery of products or services will not generate transport costs for the User and will be made to the email address registered by the User.

              Means of payment

              Payment for the products or services purchased through this website will be made by bank transfer or consignment to the Company's account, which will be informed to the User within the purchase process.

              Also, once the Company has enabled the respective channel, payments can be made virtually, for which the User may use the electronic payment system that the Company has contracted.

              Of course, the Company is exempt from liability for any damage that may be caused by any failure in the operations or communications of the virtual payment platform or of the financial institutions issuing the valid cards to make payments, with respect to those payments made by the User through the virtual mechanisms.

              Of the products and services offered The products and services offered by the Company are divided into:

              Evaluations and diagnoses: a virtual tool through which the User can count on an evaluation and diagnosis based on systemic thinking, mirror neurons and emotional intelligence, by using the license granted by the Company to access the respective questionnaire.

              The specific characteristics of this product will be made available to the User prior to the purchase decision and the conclusion of the respective contract.

              This product is delivered virtually by the Company.

              Systemic Coaching: branding service offered by the Company with the purpose of enhancing the product or service of the User.

              The specific characteristics of this product will be made available to the User prior to the purchase decision and the conclusion of the respective contract.

              This product is delivered virtually or in person by the Company, depending on the characteristics of what has been contracted by the User.

              Workshops and trainings: experience offered by the Company so that the User has an experience of development of his creativity, leadership and potential.

              The specific characteristics of this product will be made available to the User prior to the purchase decision and the conclusion of the respective contract.

              This product is delivered virtually or in person by the Company, depending on the characteristics of what has been contracted by the User.

              General delivery pledge

              The Company undertakes to the User to deliver the products or services contracted within the term established, according to the virtual or face-to-face nature of the respective delivery.

              In the case of virtual deliveries, for a product purchased through this website, the maximum delivery term will be five (5) business hours, starting from the moment of acceptance of the purchase order and verification of payment.

              In any case, the Company will inform the User, through the confirmation email of the purchase order and successful payment, the estimated time for delivery, which will not exceed twenty (20) working hours.

              In relation to face-to-face deliveries, the conditions will be specifically agreed between the User and the Company.

              However, in the event of unforeseen and irresistible circumstances beyond the control of the Company that delay or prevent delivery, such circumstances shall be reported to the User by telephone or electronic means, with the aim of achieving the best solution for the User.

              Likewise, the Company informs that it makes its best efforts to specify in detail the existing inventory, so that the User of this website knows the availability of the products. However, all purchases will be subject to the availability of inventory, so if, when the product is ready for delivery, it is not available, the Company will contact the User to resolve the situation in the best possible way.

              Returns and Exchanges Policy

              The Company has a period of ten (10) business days from the date of delivery of the order to make returns. You must notify us of the return before the end of the ten (10) business days through one of our customer service channels:

              E-mail address: [info@tuuls.com.co].

              Telephone:[=57 3234653188].

              In order for the User's request to be met, it is required that the service or product has not been used. In addition, the User must provide the original or photocopy of the sales invoice or, if the User does not have such a document, the User must request a certificate of purchase from the Company. The verification of the existence of a certificate of purchase will be carried out by the Company.

              For those products that are used in a virtual way, the Company will verify that the User has not made total or partial use of them and, if the verification is satisfactory, the Company will proceed to cancel the User's access authorization for the product that is the object of the return.
              If for inventory reasons, the Company does not have the product or products that the User requests for exchange, the Company will provide a coupon for the total value of the original purchase and redeemable within the next six (6) months, for the User to make it effective on future purchases on this website.

              Coupon to make a new purchase: this alternative will allow the User, during the following six (6) months, to use the value of his original purchase in future purchases made on this website.

              Money Back Refund: This alternative only applies to those returns that originate from quality failures of the product(s).

              The reimbursement of the money will be made by the Company, after verification of the existence of the quality failure of the product(s), and will depend on the means used for the purchase. Thus, the refund will be made by electronic transfer or to the credit card used for the original purchase.

              With the exception of the refund of the money, which applies for quality failures of the product(s), those products or services that have been customized for the User, will not be subject to refund.

              Any of the above alternatives, when the quotes are given for acceptance, will be executed by the Company within a period not to exceed ten (10) business days. This is without prejudice to the fact that the financial institutions involved use a longer term for the reimbursement of the money.

              Right of withdrawal


              Once the Company verifies the origin of the request for return of the product or products purchased by the User, the latter may choose the following alternatives:

              Change of product(s): This alternative is subject to the availability of inventory at the time of change. In any case, the change may be made for a product or products of equal or lower value than the original product or products.

              For those Users who opt for this alternative and request a product or products of lower value than the original product or products, the Company will recognize the difference in a coupon that the User may make effective during the following six (6) months, for the following year.



              In full compliance with Colombian consumer protection regulations, the Company recognizes the possibility for the User (consumer) to withdraw from the purchase made through this website.

              Consequently, the User will have five (5) business days, counted from the delivery of the product or products purchased, to proceed with the respective return in exercise of the right of withdrawal, provided that the legal requirements are met and the product or service has not been used by the User, provided that what the Company offers is an experience or a single-use license, even if several accesses are granted in the same license.

              For the purposes of this return in exercise of the right of withdrawal, the User must communicate this circumstance to the Company through the channels indicated in these terms and conditions, as well as must return the product or products purchased in the same conditions of receipt of the same, for which the content of the corresponding section in this document will be taken into account.

              In the event that the conditions for the User to exercise the right of withdrawal are met, the Company will refund the money in the terms indicated in this document.

              In no event shall the timely exercise of the right of withdrawal entail any cost to the User.

              In order for the User to know more about this right, the Company invites him/her to consult the relevant information on the website of the Superintendence of Industry and Commerce of Colombia, which can be found at: www.sic.gov.co.

              Reversal of payment

              Purchases made through this website and in which the non-presential payment mechanism has been used without authorization of the holder of the same or by virtue of fraud, obliges the participants of the payment process to make the reversal of it, when requested by the consumer.

              The same circumstance applies when the service or product delivered does not correspond to what was requested or is defective.

              If any of the above quotes is given, the User will communicate the request for reversal of the payment to the issuer of the means of payment used to make the purchase, who will contact the other participants in the payment process and, jointly, will make the reversal where appropriate.

              In any case, when the User has received the service or product, he or she must return it to the Company, for which he or she must contact the Company's customer service channels indicated in this document.

              Warranty Policy

              In order to make use of the guarantee on the service or product purchased, it will be necessary for the User to present the original or photocopy of the sales invoice or, in the absence of it, to request the proof of purchase or certificate of purchase from the Company.

              Similarly, the Company notes that the warranty on any product will be subject to a diagnosis, with the purpose of determining the existence of the anomaly and its possible cause. In any case, the answer to any guarantee request will be answered within a maximum of fifteen (15) working days.

              The guarantee will not be valid if the diagnosis issued shows:

              The existence of force majeure or fortuitous event.

              The existence of an act of a third party.


              The improper use of the good by the User or consumer.

              The inadvertence of the User or consumer regarding the instructions for use, care and maintenance indicated in the product manual and in its warranty.

              If the guarantee of the product(s) purchased is appropriate, the repair of the product(s) will be carried out completely free of charge. If the repair is not carried out, the price paid by the User will be refunded, in the terms described in this document.

              If the fault is repeated and at the User's option, a new repair will be carried out, the price paid will be returned or the product(s) will be exchanged, in accordance with the terms indicated in this document.

               The Company points out that, unless the product or products indicate a greater warranty, the warranty term will be the one indicated by the Colombian regulations in force at the time of purchase.

              Once the term of the contractual or legal guarantee expires, as applicable, the User or consumer must assume the payment of any revision, diagnosis and repair required by the purchased product or products.

              Origin of the funds

              The User solemnly declares and maintains the following:

              The resources used for the purchase come from a lawful operation, trade, profession, activity or business.

              The money used for the purchase was not obtained by virtue of any type of conduct that is enshrined in Colombian law as a misdemeanor constituting a criminal offence.

              I have never allowed third parties to use my accounts or credit or debit cards to record or manage money that I do not know where it came from or that comes from conduct that is against the law, especially criminal law.

              I release the Company from all liability for any erroneous, false or inaccurate information provided in this document or for any breach thereof, and I shall therefore be solely responsible for the same.
          </div>

          }
        </div>
      </div>
    )
  }
}
export default (Terms) 
