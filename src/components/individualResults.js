import React, { Component } from 'react';


// Components

let animals = {
};

class IndividualResults extends Component {

  componentDidMount() {
    animals.Creativo = Number(this.props.match.params.creativo)   //Leon
    animals.Sociable = Number(this.props.match.params.sociable)  // Caballo
    animals.Solidario = Number(this.props.match.params.solidario) // Condor
    animals.Visible = Number(this.props.match.params.visible) // Cisne
    animals.Visionario = Number(this.props.match.params.visionario) // halcón
    animals.Innovador = Number(this.props.match.params.innovador) // serpiente
    animals.Astuto = Number(this.props.match.params.astuto) // jaguar
    animals.Intuitivo = Number(this.props.match.params.intuitivo) // dragón
    animals.Valioso = Number(this.props.match.params.valioso) // colibrí
    animals.Generoso = Number(this.props.match.params.generoso) // ballena
    animals.Independiente = Number(this.props.match.params.independiente) //Oso
    animals.Reservado = Number(this.props.match.params.reservado) // Búho
    animals.fromIndividual = true;
    localStorage.setItem(`choices-12`, JSON.stringify(animals));
    localStorage.setItem(`email`, JSON.stringify(this.props.match.params.email));
    localStorage.setItem(`username`, JSON.stringify(this.props.match.params.username));

    localStorage.setItem(`individual`, true);
    localStorage.setItem(`preferencia`, true);
    window.location.href = 'http://impronta.tuuls.com.co/#/resultados';
  }

  componentWillMount() {
    localStorage.clear();
  }

  render() {
    const nextQuestionURL = Number(13);
    return (
      <div className="App container">
        <form action={`../#/pregunta/${nextQuestionURL}`}>
        </form>
      </div>
    );
  }
}

export default IndividualResults;

