import React, { Component } from 'react'
import { connect } from "react-redux";

import SetUserExtraInfo from '../containers/SetUserExtraInfo';

class IntroPage2 extends Component {
  render() {

    if (this.props.match.params.firstname && this.props.match.params.lastname && this.props.match.params.email && this.props.match.params.code) {
      sessionStorage.setItem('session', true);
      sessionStorage.setItem('username', this.props.match.params.firstname);
      sessionStorage.setItem('lastName', this.props.match.params.lastname);
      sessionStorage.setItem('code', this.props.match.params.code);
      sessionStorage.setItem('email', this.props.match.params.email);
      this.props.history.replace('/intro2');
    }

    return (
      <div className="Intropage2">
        {/* {sessionStorage.getItem('session') === "true" && */}
          <header className='showcase'>
            <div className="transbox">
              <div className="excerpt">
                <SetUserExtraInfo />
              </div>
            </div>
          </header>
        {/* // } */}
        {/* {(sessionStorage.getItem('session') === "false" || sessionStorage.getItem('session') === null) &&
          this.props.history.push("/")
        } */}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang
  };
}
export default connect(mapStateToProps)(IntroPage2)