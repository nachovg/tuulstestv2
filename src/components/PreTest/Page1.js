import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FormattedMessage } from 'react-intl';


//Icons
import CalcIcon from 'react-icons/lib/md/iso'

class PreTest1 extends Component {
	constructor(props) {
		super(props);

		this.state = {}

		this.handleOnChange = this.handleOnChange.bind(this)
	}

	handleOnChange(event) {
		this.setState({
			[event.target.name]: Number(event.target.value)
		})
	}

	componentDidUpdate() {
		return Object.values(this.state).reduce((a, b) => a + b, 0)
	}

	render() {
		const buttonMode = (this.componentDidUpdate() === 15) ? 'enable' : 'disabled';
		return (
			<div className="Pretest page1 container">
				{/* {sessionStorage.getItem('session') === "true" && */}
					<div>
						<div className='excerpt'>
							<p><FormattedMessage id='preTest1.pOne' defaultMessage='A continuación encontrarás una serie de palabras relacionadas con afirmaciones o preguntas acerca de ti, de lo que tú eres. Para contestar deberás repartir 15 puntos entre las palabras que mejor describan la respuesta a la pregunta o afirmación que se te hace.' /></p>
							<p><FormattedMessage id='preTest1.pTwo' defaultMessage='Antes de empezar ensayaremos para asegurarnos que conoces la mecánica de hacer el test.' /></p>
							<h2><FormattedMessage id='preTest1.pThree' defaultMessage='EJEMPLO 1 CON COLORES' /></h2>
							<p><FormattedMessage id='preTest1.pFour' defaultMessage='Reparte 15 puntos poniendo números que no sumen más de 15 puntos, pero tampoco menos, en los círculos. Entre más te guste el color, mayor debe ser el número que pones asignado al color. No todos los colores deben tener números, simplemente los que son tus favoritos. También puedes poner los 15 puntos a un solo color, pero seguramente te gustan varios, trata de ser lo más honesto y así tus resultados serán más finos y la prueba te podrá dar mayor información.' /></p>
						</div>
						<div className='circle-wrapper'>
							<ul className="circle-container">
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input1" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input2" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input3" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input4" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input5" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input6" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input7" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input8" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input9" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input10" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input11" onChange={this.handleOnChange} />
								</li>
								<li>
									<label></label>
									<input min="0" max="12" type="number" name="input12" onChange={this.handleOnChange} />
								</li>
							</ul>
						</div>
						<div className="counter">
							<h4><CalcIcon /><FormattedMessage id='preTest1.counter' defaultMessage='Contador:' /> {this.componentDidUpdate()}</h4>
							<Link
								onClick={this.handleOnClick}
								to={'/pre/2'}
								className={`btn-amarillo ${buttonMode}`}><FormattedMessage id='preTest1.nextButton' defaultMessage='Siguiente Pregunta' />
							</Link>
						</div>
					</div>
				{/* }
				{(sessionStorage.getItem('session') === "false" || sessionStorage.getItem('session') === null) &&
					this.props.history.push("/")
				} */}
			</div>

		)
	}
}

export default PreTest1