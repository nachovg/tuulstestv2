import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FormattedMessage } from 'react-intl';

//Icons
import CalcIcon from 'react-icons/lib/md/iso'

class PreTest2 extends Component {
	constructor(props) {
		super(props);

		this.state = {}

		this.handleOnChange = this.handleOnChange.bind(this)
	}

	handleOnChange(event) {
		this.setState({
			[event.target.name]: Number(event.target.value)
		})
	}

	componentDidUpdate() {
		return Object.values(this.state).reduce((a, b) => a + b, 0)
	}


	render() {
		const buttonMode = (this.componentDidUpdate() === 15) ? 'enable' : 'disabled';
		return (
			<div className="Pretest page2 container">
				{/* {sessionStorage.getItem('session') === "true" && */}
					<div>
						<div className='excerpt'>
							<h2><FormattedMessage id='preTest2.pOne' defaultMessage='Pregunta de ensayo 2' /></h2>
							<p><FormattedMessage id='preTest2.pTwo' defaultMessage='Reparte 15 puntos poniendo números en los círculos. Entre más te identifiques con la palabra que aparece frente al círculo, SEGÚN LA AFIRMACIÓN, más alto debes poner el puntaje. No todas las palabras deben tener números, simplemente se califican las que describen mejor lo que eres. También puedes poner los 15 puntos en una sola palabra, pero seguramente te identificas con varias, trata de ser lo más honesto y así tus resultados serán más finos y la prueba te podrá dar mayor información acerca de tus preferencias.' /></p>
							<h1><FormattedMessage id='preTest2.pThree' defaultMessage='Cuando estoy en mi casa soy:' /></h1>
						</div>
						<div className='circle-wrapper'>
							<ul className="circle-container">
								<li>
									<label><FormattedMessage id='preTest2.Creativo' defaultMessage='Creativo' /></label>
									<input min="0" max="12" type="number" name="input1" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Sociable' defaultMessage='Sociable' /></label>
									<input min="0" max="12" type="number" name="input2" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Valioso' defaultMessage='Valioso' /></label>
									<input min="0" max="12" type="number" name="input3" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Visible' defaultMessage='Visible' /></label>
									<input min="0" max="12" type="number" name="input4" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Visionario' defaultMessage='Visionario' /></label>
									<input min="0" max="12" type="number" name="input5" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Innovador' defaultMessage='Innovador' /></label>
									<input min="0" max="12" type="number" name="input6" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Astuto' defaultMessage='Astuto' /></label>
									<input min="0" max="12" type="number" name="input7" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Intuitivo' defaultMessage='Intuitivo' /></label>
									<input min="0" max="12" type="number" name="input8" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Solidario' defaultMessage='Solidario' /></label>
									<input min="0" max="12" type="number" name="input9" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Generoso' defaultMessage='Generoso' /></label>
									<input min="0" max="12" type="number" name="input10" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Independiente' defaultMessage='Independiente' /></label>
									<input min="0" max="12" type="number" name="input11" onChange={this.handleOnChange} />
								</li>
								<li>
									<label><FormattedMessage id='preTest2.Reservado' defaultMessage='Reservado' /></label>
									<input min="0" max="12" type="number" name="input12" onChange={this.handleOnChange} />
								</li>
							</ul>
						</div>
						<div className="counter">
							<h4><CalcIcon /><FormattedMessage id='preTest1.counter' defaultMessage='Contador: ' /> {this.componentDidUpdate()}</h4>

							{buttonMode === 'enable' &&
								<div>
									<h4><FormattedMessage id='preTest2.pFour' defaultMessage='¡Ahora sí, vamos a hacer el test!' /></h4>
									<p><FormattedMessage id='preTest2.pFive' defaultMessage='Son 12 preguntas o afirmaciones y debes contestar siguiendo la mecánica que se te acaba de explicar.' /></p>
								</div>
							}

							<Link
								to={'/pregunta/1'}
								className={`btn-amarillo ${buttonMode}`}><FormattedMessage id='preTest2.startButton' defaultMessage='Empezar el Test' />
							</Link>

						</div>
					</div>
				{/* }
				{(sessionStorage.getItem('session') === "false" || sessionStorage.getItem('session') === null) &&
					this.props.history.push("/")
				} */}
			</div>

		)
	}
}

export default PreTest2	