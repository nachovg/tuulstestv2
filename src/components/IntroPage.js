import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import SetUser from '../containers/SetUser'

class IntroPage extends Component {
	render() {
		return (
			<div className="Intropage">
				<header className='showcase'>
					<div className="transbox">
						<Link to='/'><h1>Tuuls</h1></Link>
						<div className="excerpt">
							<h2>Hacer el test<hr id='borderYellow' /></h2>
							<br />
							<SetUser />
						</div>
					</div>
				</header>
			</div>
		)
	}
}

export default IntroPage