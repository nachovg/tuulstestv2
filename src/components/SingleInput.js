import React, { Component } from 'react';

export default class Input extends Component {
	constructor(props) {
		super(props);

		this.state = {
			assignedValue: '',
		}

		this.handleOnChange = this.handleOnChange.bind(this);
	}

	componentWillMount() {
		const inputName = this.props.name;
		const parentState = this.props.parentState;
		//si encuentra inputName en parentState lo asigna como su valor local.
		if (parentState.hasOwnProperty(inputName)) {
			this.setState({
				assignedValue: parentState[inputName],
			});
		}
	}

	render() {
		return (
			<input
				type='number' min='0' max='12'
				name={this.props.name} //se lo manda InputList.js
				value={this.state.assignedValue}
				onChange={this.handleOnChange}
			/>
		)
	}

	handleOnChange(event) {
		if (event.target.value[0] === '0') {
			this.setState({ assignedValue: event.target.value[1] });
		} else {
			this.setState({ assignedValue: Number(event.target.value) });
		}
		return (
			//le regresa dos valores al padre (InputList) de 'name' y 'value'.
			this.props.callbackFromParent(this.props.name, Number(event.target.value))
		);
	}

}

