export const LOCALE_SET = 'LOCAL_SET';


export const localeSet = lang => ({
  type: LOCALE_SET,
  lang
})

export const setLocale = lang => (dispatch) => {
  sessionStorage.setItem(["lang"], lang)
  dispatch(localeSet(lang))
}