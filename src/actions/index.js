import { animales, animalesOrdenados } from '../components/Results/dataFromLocalStorage'
import userData from '../components/Results/dataFromSeccionStorage'

export let SUCCESS = 'SUCCESS';
export let FAIL = 'FAIL';
export let FETCH_USER = 'FETCH_USER';
export let RECEIVE_SERVER_ERROR = 'RECEIVE_SERVER_ERROR';
export let FETCHING = 'FETCH_USER';
export let CHECKCODESUCCESS = 'CHECKCODESUCCESS';
export let CHECKCODEFAIL = 'CHECKCODEFAIL';

export function counter(value) {
	return {
		type: 'COUNTER',
		value
	};
}

export function complete(recomendation, utility, representation) {
	return {
		type: 'COMPLETE',
		value: true,
		recomendation,
		utility,
		representation
	};
}

export function setuser(user) {
	return {
		type: 'SETUSER',
		user
	};
}

function receiveServerError() {
	return {
		type: FAIL,
		fail: true,
		success: false
	}
}

// function receiveUserReepeted() {
// 	return {
// 		type: FAIL,
// 		fail: true
// 	}
// }

// function receiveServerErrorSaved() {
// 	return {
// 		type: FAIL,
// 		fail: true,
// 		success: false
// 	}
// }


function receiveRightAnswerFromServer() {
	return {
		type: SUCCESS,
		fail: false,
		success: true
	}
}

function checkCodeSucess() {
	return {
		type: CHECKCODESUCCESS,
		match: true
	}
}

function checkCodeFail() {
	return {
		type: CHECKCODEFAIL,
		match: false
	}
}

export function fetchAllData(resultados) {
	return (dispatch) => {
		let bday = '';
		let localStorageIntoArray = Object.values(localStorage).map((item, i) => {
			let allItems = localStorage["choices-" + i].replace(/}/, `, "question": ${i + 1}}`)
			return (JSON.parse(allItems))
		});

		if (userData.birthday !== undefined) {
			bday = userData.birthday.substring(3, 16);
		}

		fetch('https://api.tuuls.com.co/api/userData', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				username: userData.username + " " + userData.lastName,
				email: userData.email,
				code: userData.code,
				// new data
				age: bday,
				country: userData.country,
				education: userData.education,
				employment: userData.employment,
				gender: userData.gender,
				region: userData.region,
				status: userData.status,
				// new data
				Jaguar: animales.Jaguar,
				Dragon: animales.Dragón,
				Colibri: animales.Colibrí,
				Cisne: animales.Cisne,
				Halcon: animales.Halcón,
				Serpiente: animales.Serpiente,
				Caballo: animales.Caballo,
				Leon: animales.León,
				Condor: animales.Cóndor,
				Ballena: animales.Ballena,
				Oso: animales.Oso,
				Buho: animales.Búho,
				firstAnimalPercentage: animalesOrdenados[0][0],
				secondAnimalPercentage: animalesOrdenados[1][0],
				individualResults: localStorageIntoArray,
				currentDate: new Date().toJSON().slice(0, 10).replace(/-/g, '-')
			})
		}).then((response) => {
			if (!response.ok) {
				throw new Error('Bad response from server');
			}
			return response
		}).then(response => response.json())
			.then(response => {
				if (!response.success) {
					dispatch(receiveServerError());
				}
				else {
					localStorage.setItem(`notSend`, true);
					sessionStorage.clear();
					dispatch(receiveRightAnswerFromServer());
				}
			}).catch(error => dispatch(receiveServerError(error))
			);
	}
}

export function userCodeCheck(codeToMatch) {
	return (dispatch) => {
		let currentEmail = document.getElementById('emailCode').value;
		// fetch("https://api.tuuls.com.co/api/check/code", {
		// 	method: "POST",
		// 	headers: {
		// 		'Accept': 'application/json',
		// 		'Content-Type': 'application/json',
		// 	},
		// 	body: JSON.stringify({
		// 		"email": currentEmail,
		// 		"code": codeToMatch
		// 	})
		// }).then(res => res.json())
		// 	.then(res => {
		// 		if (res.status === 200) {
		// 			sessionStorage.setItem('session', true);
					dispatch(checkCodeSucess());
			// 	} else if (res.status === 405) {
			// 		alert(res.msg)
			// 	} else {
			// 		dispatch(checkCodeFail());
			// 	}
			// }
			// )
			// .catch(err => console.error(err))
	}
}
